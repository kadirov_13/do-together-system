<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('tel_number');
            $table->string('login');
            $table->string('type');
            $table->integer('online_ball');
            $table->integer('image_id');
            $table->string('password');
            $table->string('country');
            $table->integer('usertype_id');
            $table->integer('gpgroup_id');
            $table->integer('gpsch_id');
            $table->integer('gplcenter_id');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
