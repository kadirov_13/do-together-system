<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('exam');
            $table->string('theme');
            $table->string('a_answer');
            $table->string('b_answer');
            $table->string('c_answer');
            $table->string('d_answer');
            $table->string('true_answer');
            $table->integer('subject_id');
            $table->integer('ball');
            $table->integer('true_answers');
            $table->integer('utf_id');
            $table->string('name_test');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tests');
    }
}
