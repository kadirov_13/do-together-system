<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGpschesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gpsches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->string('city');
            $table->integer('reyting');
            $table->string('login');
            $table->string('password');
            $table->integer('image_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gpsches');
    }
}
