<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlimpiadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olimpiadas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->dateTime('begin_time');
            $table->dateTime('length');
            $table->string('subject_id');
            $table->integer('olimpiada_id');
            $table->integer('have_answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('olimpiadas');
    }
}
