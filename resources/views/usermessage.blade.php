@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Barcha yo'llangan so'rovlar jadvali</p>
			@if(Auth::check())
			<?php $name = Auth::user()->lastname.'  '.Auth::user()->firstname ?>
			<p><i class="fa fa-user-circle"></i><b>{{ $name }}</b> ga jo'natilgan barcha jo'natmalar</p>
			@endif
		</div>

	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> <i class="fa fa-list-ol"></i>№</th>
					<th><i class="fa fa-calendar"></i> So'rov bayoni</th>
					<th><i class="fa fa-exclamation-circle"></i>Javob yo'llash</th>
				</tr>
			</thead>
			<tbody>
				<?php $number = 1; ?>
				@foreach($messages as $message)						   
				<tr>
					<td>{!! $number++ !!}</td>
					<td><a href="#" style="font-size: 13px;">{!! $message->messages !!}</a></td>
					<td style="max-width: 5em;">
						<div class="row" style="padding-top: 1em;">
							<div class="col-md-6">
								<div class="tg-ok">
									<a class="tg-dd" href="{{ route('actionCompetitionanswer',['answer'=>1,'message'=>$message->id]) }}">
										<span>
											<i class="fa fa-thumbs-up"></i> Qabul qilish
										</span>
									</a>
								</div>
							</div>
							<div class="col-md-6">
								
								<div class="tg-ok">
									<a class="tg-ko" href="{{ route('actionCompetitionanswer',['answer'=>0,'message'=>$message->id]) }}">
										<span>
											<i class="fa fa-thumbs-down"></i> Rad  etish 
										</span>
									</a>
								</div>
							</div>	
						</div>
					</td>
				</tr>						    
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="moreinfonumber">
		{!! $messages->links() !!}
	</div>
</div>	
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')