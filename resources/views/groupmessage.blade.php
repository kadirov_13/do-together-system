@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Barcha yo'llangan so'rovlar jadvali</p>
			@if(Auth::check())

			<p><i class="fa fa-user-circle"></i><b></b>Guruhga jo'natilgan barcha jo'natmalar</p>
			@endif
		</div>

	</div>
	<div class="table">
					@if(Session::has('fail'))
						<h6 style="color: red;font-size: 13px;" id="error" class="text-center"> {{ Session::get('fail') }} </h6>
					@endif	
			<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> <i class="fa fa-list-ol"></i>№</th>
					<th><i class="fa fa-calendar"></i> So'rov bayoni</th>
					<th><i class="fa fa-exclamation-circle"></i>Javob yo'llash</th>
				</tr>
			</thead>
			<tbody>
				<?php $number = 1; ?>
				@foreach($messages as $message)	
				<?php
				$group = App\Gpgroup::where('id',$message->group_id)->first();
				// echo($group);
				if(is_null($group)){
					$name = 'topilmadi';
				}
				else
				{
					$name = $group->name;
			  }
			  $user = App\User::where('id',$message->user_id)->first();
					?>					   
					<tr>
						<td>{!! $number++ !!}</td>
						<td>
							<i style="font-size: 13px;"><b>{!! $user->firstname !!} {!! $user->lastname !!}</b> sizda <p>guruhingizga qo'shishingizni so'rayabdi.</p>
							</i>
						</td>
						<td style="max-width: 5em;">
							@if( Auth::user()->id == $group->admin_id or Auth::user()->type == 'admin')
							<div class="row" style="padding-top: 1em;">
								<div class="col-md-6">
									<div class="tg-ok">
										<a class="tg-dd" href="{{ route('actionGroupmessageanswer',['answer'=>1,'id'=>$message->id]) }}">
											<span>
												<i class="fa fa-thumbs-up"></i> Qo'shish
											</span>
										</a>
									</div>
								</div>
								<div class="col-md-6">

									<div class="tg-ok">
										<a class="tg-ko" href="{{ route('actionGroupmessageanswer',['answer'=>0,'id'=>$message->id]) }}">
											<span>
												<i class="fa fa-thumbs-down"></i> Rad  etish 
											</span>
										</a>
									</div>
								</div>	
							</div>
							@else
							<span>Amal yo'q</span>
							@endif
						</td>
					</tr>						    
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="moreinfonumber">
			{!! $messages->links() !!}
		</div>
	</div>	
	@endsection('content')

	@section('menyu')
	@include('includes.mainmenyu')
	@endsection('menyu')