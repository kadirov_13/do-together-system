				<div class="category">
					<div class="headcategory">
						<span>Bo'limlar</span>
					</div>
					<div class="menyucategory">
						<div class="menyu">
							<ul>
								<li>
									<a href="{{ route('actionSubject') }}" class="activemenyu">
										<i class="fa fa-home"></i> Yangiliklar
									</a>
								</li>
								<li class="outer">
									<a href="{{ route('actionTest') }}">
										<i class="fa fa-text-height"></i>Testlar
									</a>
									<div class="line_horizontal"></div>
								</li>
								<li class="outer">
									<a href="{{ route('actionStatus')}}">
										<i class="fa fa-comments"></i>Jo'natmalar
									</a>
									<div class="line_horizontal"></div>
								</li>
								<li class="outer">
									<?php 
									if(Auth::check())
										$id =Auth::user()->id;
									else
										$id = 0;
									// echo ($id);
									?>
									<a href="{{ route('actionUserinfo',['id'=>$id,'reting'=>0] ) }}">
										<i class="fa fa-user-circle-o"></i>Shaxsiy blok
									</a>
									<div class="line_horizontal"></div>
								</li>
								<li class="outer">
									<a href="{{ route('actionReting') }}">
										<i class="fa fa-trophy"></i> Reyting
									</a>
									<div class="line_horizontal"></div>
								</li>
								<li class="outer">
									<a href="{{ route('actionAllolimpiadas') }}">
										<i class="fa fa-globe"></i> Online olimpiadalar
									</a>
									<div class="line_horizontal"></div>
								</li>
								<li class="outer">
									<a href="{{ route('ActionOnlineUsers') }}">
										<i class="fa fa-users"></i> Hozir online
									</a>
									<div class="line_horizontal"></div>
								</li>
								<li class="outer">
									<a href="{{ route('actionGroupslist') }}">
										<i class="fa fa-glide"></i> Guruhlar
									</a>
									<div class="line_horizontal"></div>
								</li>
								<li class="outer">
									<a href="{{ route('actionAnswer') }}">
										<i class="fa fa-check-circle"></i> Yechimlar
									</a>
									<div class="line_horizontal"></div>
								</li>
								<li class="outer">
									<a href="#">
										<i class="fa fa-question-circle"></i> Yordam
									</a>
									<div class="line_horizontal"></div>
								</li>
								<li class="outer">
									<a href="{{ route('actionBooks') }}">
										<i class="fa fa-book"></i> Darsliklar
									</a>
									<div class="line_horizontal"></div>
								</li>

							</ul>
						</div>		
					</div>
					<div class="headcategory">
						<span>Statistika</span>
					</div>
					<div class="menyucategory">
						<div class="menyu">
							<ul>
								<li>
									<span>
										<i class="fa fa-rebel"></i>Jami testlar :
										<strong>  239 ta</strong>
									</span>
								</li>
								<li>
									<span>
										<i class="fa fa-users"></i>Jami foydalanuvchilar :
										<strong> 393 ta</strong>
									</span>
								</li>
								<li>
									<span>
										<i class="fa fa-envelope"></i> Yangiliklar :
										<strong> 35 ta</strong>
									</span>
								</li>
								<li>
									<span>
										<i class="fa fa-users"></i>Hozirda online :
										<strong> 19 ta</strong>
									</span>
								</li>
								<li>
									<span>
										<i class="fa fa-comments"></i> Yangiliklarga izohlar :
										<strong> 15 ta</strong>
									</span>
								</li>
							</ul>
						</div>		
					</div>
				</div>