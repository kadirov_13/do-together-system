<div class="container-fluid">
	<div class="headsubject">
		<div class="container" style="margin-top: -0.6em;">
			<div class="row">
				<div class="col-md-9 col-sm-12">
					<div class="row">
						<div class="col-md-6">
							<div class="logo">
								<a href="{{ route('actionIndex') }}"> <img src="{{ URL::to ('src/')}}/img/logo/innew.png"/>
								</a>
							</div>
							<div class="subjectname">
								<span>
									<strong>{{ App\Subject::where('id',Session::get('subject_id','1'))->first()->name }}</strong>
								</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="usersname">
								@if(Auth::check())
									<i class="fa fa-user"></i>
									<span>{{ Auth::user()->firstname }}  {{ Auth::user()->lastname}}</span>
								@else
									<i class="fa fa-user"></i>
									<span>Siz xali profilingizga kirmagansiz </span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="login">
						@if(Auth::check())
							<a href=" {{ route('logout')}}">
								<i class="fa fa-sign-out"></i>Chiqish
							</a>
						@endif
						@if(!Auth::check())
							<a href=" {{ route('bestlogin') }}">
								<i class="fa fa-sign-in"></i>Kirish
							</a>
						@endif
						
					</div>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-12">
					<nav class="navv">
							<div class="subjects">
								<ul class="view" >
									<i class="fa fa-align-justify" style="color: #3864ba;" onclick="on()"><span>   Fanlar</span></i>
								</ul>
								<ul id="navul">
									<li><a href="{{ route('getsubject',['id' =>2]) }}">Matematika</a></li>
									<li><a href="{{ route('getsubject',['id' => 1]) }}">Informatika</a></li>
									<li><a href="{{ route('getsubject',['id' =>3]) }}">Fizika</a></li>
									<li><a href="{{ route('getsubject',['id' =>4]) }}">Kimyo</a></li>
									<li><a href="{{ route('getsubject',['id' =>5]) }}">Biologiya</a></li>
									<li><a href="{{ route('getsubject',['id' =>6]) }}">Tarix</a></li>
									<li><a href="{{ route('getsubject',['id' =>7]) }}">Onatili va Adabiyod</a></li>
									<li><a href="{{ route('getsubject',['id' =>8]) }}">Engish</a></li>
									<li><a href="{{ route('getsubject',['id' =>9]) }}">Rus tili</a></li>
								</ul>
							</div>
							<!-- /.navbar-collapse -->
					</nav>
				</div>
			</div>

		</div>
			
	</div>

</div>
<script type="text/javascript">
	var ul = document.getElementById('navul');

	function on(){
		// alert('salom');
		if( ul.style.display=='block')
	 		ul.style.display = 'none';
	 	else
	 		ul.style.display = 'block';

	}

</script>
