@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<div class="row">
				<div class="col-md-7">
					<p>Guruh haqidagi ma'lumotlar</p>
				</div>	
				<div class="col-md-5">
					
					<div class="row" style="color: #3864ba">
					<div class="col-md-4" >
						
						<a href="">
							<p class="text-center gpmenyu">
								<i class="fa fa-paper-plane-o"> </i>
							</p>
						</a>
					</div>
					<div class="col-md-3">
						<a href="#">
							<p class="text-center gpmenyu">
								<i class="fa fa-bar-chart"> </i>
							</p>	
						</a>
					</div>
					<div class="col-md-5">
						<a href="{{ route('actionGroupmessages',['id'=>$group->id]) }}">
							<p class="text-center gpmenyu">
								<i class="fa fa-commenting-o">
									
								</i>
								<strong>
									@if($count_messages!=0)
									<span class="count_messages">+{!! $count_messages !!} </span>
									@endif
								</strong>
							</p>
						</a>
					</div>

				</div>

				</div>
			</div>
				

		</div>

	</div>
	<?php
		
	    $count_users = count($users);
	    $reyting=1;

	?>
	<div class="newscontent">
		<div class="userinfo">
			<div class="row">
				<div class="col-md-4">
					<div class="imguser">
					
						<img src="{{ URL::to($group->image->path) }}">
					
					</div>
					<div class="username">
						<p>
							{!!$group->name !!}
						</p>
						<div class="seti">
							<a href="">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="">
								<i class="fa fa-instagram"></i>
							</a>						
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="infocontent">
						<div class="resoult">
							<table class="table table-striped table-bordered table-hover ">
								<tr>

									<td class="left_td"><i class="fa fa-user"></i> Nomi: </td>
									<td> {{$group->name}}</td>
								</tr>

								<tr>

									<td class="left_td"><i class="fa fa-trophy"></i> Reyting: </td>
									<td> {{$group->reyting}}</td>
								</tr>
								<tr>
									<td class="left_td"><i class="fa fa-users"></i>Azolar soni</td>
									<td>{!! $count_users !!}</td>
								</tr>
								<tr>
									<td class="left_td"><i class="fa fa-globus"></i> Viloyati</td>
									<td>{!! $group->city !!}</td>
								</tr>
								
							</table>
						</div>
					</div>
				</div>
			</div>
				<div class="row">
						@if(Auth::check())
					
					<div class="col-md-12">
						<?php $id = Auth::user()->gpgroup_id; ?>
						@if($id==$group->id or Auth::user()->usertype->type == 'admin' )
						<form action="{{ route('userimage') }}" method="POST" enctype="multipart/form-data">
							{!! csrf_field() !!}
							<div class="formimg"> 
								<p>Rasm qo'yish</p>
								<input type="file" name="image" class="fileimg">
								<input type="text" name="reting" style="display: none;" value="{{ $group->reyting }}">
								<div class="login try">
									<button type="submit">
										<i class="fa fa-image"></i>
										Qo'yish
									</button>
								</div>	
							</div>
						</form>

					@endif
					</div>
					@endif

				</div>
				<div class="row">
						<div class="searchcontent">
							<div class="retingsearch">
								<p>Guruh azolari.</p>					
							</div>

						</div>
					<div class="col-md-12">
						<table  class="table table-striped table-bordered table-hover ">
							<thead>
								<tr>
							<th> <i class="fa fa-trophy"></i> № </th>
							<th> <i class="fa fa-user"></i> Foydalanuvchi </th>
							<th><i class="fa fa-globe"></i>Viloyati</th>
							<th>To'liq ma'lumotlar</th>
						</tr>

							</thead>
							<tbody>
						@foreach($users as $user)
						<tr>							
						<td> {{ $reyting++ }}</td>
						<td>
							<a href="{{ route('actionUserinfo',['id'=> $user->id,'n'=>0]) }}">{!! $user->lastname !!}
								{!! $user->firstname !!}
							</a>
						</td>
						<td>{{ $user->country }}</td>
						<td>
							<div class="tg-bannercontent">
								<a class="tg-btn" href="{{ route('actionUserinfo',['id'=> $user->id,'n'=>0]) }}">
									<span>
										 Ko'rish.
									</span>
								</a>
							</div>
						</td>
					</tr>
					@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

	</div>
	<div class="bor"></div>

</div>
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
