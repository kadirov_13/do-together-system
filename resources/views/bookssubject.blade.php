@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')

@section('content')

<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Fandagi mavjud kitoblar </p>
		</div>
		<div class="search">
			<div class="inputb">
			</div>
		</div>
	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> 
						<i class="fa fa-list-ol"></i>№
					</th>
					<th> 
						<i class="fa fa-edit"></i> Nomi 
					</th>
					<th><i class="fa fa-arrow-circle-down"></i>Yuklash</th>
				</tr>
			</thead>
			<tbody>	
				<?php $number = 1; ?>
				@foreach($books as $book)				   
				<tr>
					<td>{!! $number++ !!}</td>
					<td><a href="{{ $book->path }}">{!! $book->name !!}</a></td>
					<td>
						<a href="{{ $book->path }}">
							<img src="{{ URL::to('src/img/save1.png') }}"/>
						</a>
					</td>
				</tr>	
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="moreinfonumber">

	</div>
</div>
@endsection('content')
@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')