@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')
@if(Auth::check())
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			@if(Auth::user()->id == $ufb->user->id or Auth::user()->usertype->type == 'admin')
			<div class="row">	
				<div class="col-md-4">	
					<div class="groupuser">
						<i class="fa fa-user"></i>
					</div>
					<div class="tg-bannercontent ">
						<a class="tg-btn" href="{{ route('actionGroupinfo',['id'=>$ufb->user->gpgroup_id]) }}">
							<span>
								<b>Mening guruhim</b>
							</span>
						</a>
					</div>
				</div>
				@if(is_null($messagegroup))
				<div class="col-md-4">	
					<div class="groupuser">
						<i class="fa fa-plus-square "></i>
					</div>
					<div class="tg-bannercontent ">
						<a class="tg-btn" href="{{ route('actionGpgroup') }}">
							<span>
								<b>Guruh ochish</b>
							</span>
						</a>
					</div>
				</div>
				<div class="col-md-4">	
					<div class="groupuser">
						<i class="fa fa-users"></i>
					</div>
					<div class="tg-bannercontent ">
						<a class="tg-btn" href="{{ route('actionGroupslist') }}">
							<span>
								<b>Guruhlar bo'limi</b>
							</span>
						</a>
					</div>
				</div>
				@else
				<div class="col-md-8">
					<?php $groupname = App\Gpgroup::where('id',$messagegroup->group_id)->first()->name; ?>
					<p>
						<i>Siz <b>{{ $groupname }}</b> guruhiga qabul qilindingiz !!!</i>
						<div class="tg-bannercontent " style="float: right;">
						<a class="tg-btn" href="{{ route('usergroupyes',['id'=>$messagegroup->id,'gp'=>$messagegroup->group_id ]) }}">
							<span>
								<b>Tasdiqlash</b>
							</span>
						</a>
					</div>
					</p>
				</div>
				@endif
			</div>			
			@else

			<p>Foydalanuvchi haqidagi ma'lumotlar !!!</p>


			@endif

		</div>

	</div>
	<div class="newscontent">
		<div class="userinfo">
			<div class="searchcontent">
				<div class="retingsearch">
					<p>
						<i class="fa fa-book"></i><b> {{ $ufb->subject->name }}</b> fani boyicha natijalar
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="imguser">

						<img src="{{ URL::to($ufb->user->image->path) }}">

					</div>
					<div class="username">
						<p>{!!$ufb->user->firstname !!}
							{!!$ufb->user->lastname !!}
						</p>
						<div class="seti">
							<a href="">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="">
								<i class="fa fa-instagram"></i>
							</a>						
						</div>
					</div>

				</div>
				<div class="col-md-8">
					<div class="infocontent">
						<div class="resoult">
							<table class="table table-striped table-bordered table-hover ">
								@if ($reting!=0)
								<tr>
									<td class="left_td"><i class="fa fa-trophy"></i> O'rni : </td>
									<td> {{$reting}}</td>
								</tr>
								@endif
								<tr>
									<td class="left_td"><i class="fa fa-envelope"></i> Jami jo'natmalar</td>
									<td>{!! $ufb->count_answer !!}</td>
								</tr>
								<tr>
									<td class="left_td"><i class="fa fa-check"></i> To'gri javoblar</td>
									<td>{!! $ufb->count_true_answer !!}</td>
								</tr>
								<?php $count_wrong =  $ufb->count_answer-$ufb->count_true_answer    ?>
								<tr>
									<td class="left_td"><i class="fa fa-times-circle"></i> Noto'g'ri javoblar soni </td>
									<td>{{ $count_wrong }}</td>
								</tr>
								<tr>
									<td class="left_td"><i class="fa fa-edit"></i> Jami to'plangan ball</td>
									<td>{!! $ufb->ball !!}</td>
								</tr>
								<tr>
									<td class="left_td"><i class="fa fa-calendar"></i> So'ngi tashrif</td>
									<td>{{ $ufb->lasttime }}</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?php $id = Auth::user()->id; ?>
						@if($id==$ufb->user_id or Auth::user()->usertype->type == 'admin' )
						<form action="{{ route('userimage') }}" method="POST" enctype="multipart/form-data">
							{!! csrf_field() !!}
							<div class="formimg"> 
								<p>Rasm qo'yish</p>
								<input type="file" name="image" class="fileimg">
								<input type="text" name="reting" style="display: none;" value="{{ $reting }}">
								<div class="login try">
									<button type="submit">
										<i class="fa fa-image"></i>
										Qo'yish
									</button>
								</div>	
							</div>
						</form>

						@endif
					</div>
				</div>
			</div>
			<!-- <div class="nameexam"> -->
				<div class="row" style="color: #3864ba">


					<div class="col-md-3" >
						@if(Auth::user()->id == $ufb->user->id or Auth::user()->usertype->type == 'admin')
						<a href="{{ route('actionUserstatus',['id'=>$ufb->user_id]) }}">
							<p class="text-center">
								<i class="fa fa-paper-plane-o" style="font-size: 3em"> </i>
							</p>
							<p class="text-center">
								<strong>
									<i >Jo'natmalar</i>
								</strong>
							</p>
						</a>
						@endif	
					</div>
					<div class="col-md-3">
						<a href="{{ route('truetests',['id'=>$ufb->user_id,'n'=>$reting]) }}">
							<p class="text-center">
								<i class="fa fa-check-circle" style="font-size: 3em"> </i>
							</p>
							<p class="text-center">
								<strong>
									<i >Topilgan testlar</i>
								</strong>
							</p>
						</a>
					</div>
					<div class="col-md-3">
						<a href="{{ route('actionallCompetitionuser',['id'=>$ufb->user_id]) }}">
							<p class="text-center">
								<i class="fa fa-bar-chart" style="font-size: 3em"> </i>
							</p>
							<p class="text-center">
								<strong>
									<i>Bellashuvlar</i>
								</strong>
							</p>	
						</a>
					</div>
					<div class="col-md-3">
						@if(Auth::user()->id==$ufb->user_id or Auth::user()->usertype->type == 'admin')
						<a href="{{ route('actionMessages',['id'=>$ufb->user_id]) }}">
							<p class="text-center">
								<i class="fa fa-commenting-o" style="font-size: 3em"> </i>
							</p>
							<p class="text-center">
								<strong>
									@if($count_messages!=0)
									<i >Xabarlar</i><span class="count_messages">+{!! $count_messages !!}ta</span>
									@else
									<i >Xabarlar</i>
									@endif
								</strong>
							</p>
						</a>
						@endif	
					</div>

				</div>
			</div>
		</div>
		<div class="bor"></div>
		<div class="newcontent">
			<div class="userinfo">
				@if($t==1)
				<span><i style="color: #3864ba">To'g'ri topilgan savollar</i></span>
				<div class="trueanswers">
					<?php $id = $ufb->subject_id; ?>
					@foreach($utfs as $utf )
					@if($utf->test->subject_id==$id)
					<a href="{{ route('actionTestexam',['id'=>$utf->test->id]) }}">{!! $utf->test->name_test !!}</a>
					@endif
					@endforeach
				</div>
				@endif

			</div>

		</div>

	</div>

	@else
	<div class="searchcontent">
		<div class="retingsearch">
			<p> <i class="fa fa-question"></i> Siz blokga kirish uchun ro'yhatdan o'tishingiz , o'z profilingizga kirishingiz va kamida bitta testga javob yo'llagan bo'lishingiz lozim !!!
				<a class="tg-btn" href="{{ route('registration') }}">
					<span>
						<i class="fa fa-registered"></i> Ro'yxatdan o'tish
					</span>
				</a>
			</p>
		</div>
	</div>
	@endif
	@endsection('content')

	@section('menyu')
	@include('includes.mainmenyu')
	@endsection('menyu')
	@section('script')

	<script type="text/javascript">
	</script>
	@endsection('script')