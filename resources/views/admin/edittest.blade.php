@extends('layout.main')

@section('header')
	@include('includes.header1')
@section('header')

@section('content')

				<div class="mainnews" >
					<div class="searchcontent">
						<div class="retingsearch">
							<p>Testni o'zgartirish</p>
						</div>
						<div class="search">
							<div class="inputb">

							</div>
						</div>
					</div>
					<div class="table" style="background-color: #fff">
						<form action="{{ route('edittest') }}" method="post">
							<div class="addtema" >
								<span>Fan : </span>
								<select class="subjecttest" name="subject">
									@foreach($subjects as $subject)
									<option>{!! $subject->name !!}</option>
									@endforeach
								</select>
							</div>
							<div class="addtema" >
								<span>Olimpiada : </span>
								<select class="subjecttest" name="olimpiada">
									@foreach($olimpiadas as $olimpiada)
									<option>{!! $olimpiada->name !!}</option>
									@endforeach
								</select>
							</div>
						<div class="addtema">
							<span>Nomi : </span>
							<input type="text" name="name_test" value="{{ $test->name_test }}">
						</div>
						<div class="addtema">
							<span>Mavzu : </span>
							<input type="text" name="theme" value="{{$test->theme}}">
							<input type="text" value="{{ $test->id }}" name="test_id" style="display: none;
							">
						</div>
						<div class="form-group">
							<span>Savol :</span>	
							<textarea class="form-control" name="exam" id="input" rows="10">{!! $test->exam !!}</textarea>
						</div>
						<div class="retingsearch searchcontent">
							<p>Varyantlar</p>
						</div>
						<div class="exam2">
							<span>A)</span>
							<input type="text" name="a_var" value="{{$test->a_answer}}">
						</div>
						<div class="exam2">
							<span>B)</span>
							<input type="text" name="b_var" value="{{$test->b_answer}}">
						</div>
						<div class="exam2">
							<span>C)</span>
							<input type="text" name="c_var" value="{{$test->c_answer}}">
						</div>
						<div class="exam2">
							<span>D)</span>
							<input type="text" name="d_var" value="{{$test->d_answer}}">
						</div>
						<div class="exam1">
							<span>To'g'ri javob :</span>
							<select class="subjecttest" name="true_answer">
								<option>A</option>
								<option>B</option>
								<option>C</option>
								<option>D</option>
							</select>
						</div>
						<div class="exam2">
							<span>Ball : </span>
							<input type="text" name="ball" value="{{ $test->ball }}">
						</div>
						{!! csrf_field() !!}
						<div class="exam3">
							<button type="submit" style="max-height: 60px;">
							 <i class="fa fa-plus"></i>Saqlash
							</button>
						</div>
					</form>
					</div>
				</div>

@endsection('content')

@section('menyu')
	@include('includes.mainmenyu')
@endsection('menyu')

@section('script')
<!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=5x3ngooqcfd46b27y4kxt79abx577l8m03dus6pqstdlktdz
"></script> -->
    <script type="text/javascript">
      tinymce.init({
      selector: 'textarea',
      theme: 'modern',
      plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
      toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      powerpaste_word_import: 'merge',
     });
    </script>
@endsection('script)