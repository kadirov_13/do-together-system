@extends('layout.main')

<div class="container-fluid">
	<div class="container">
		<div class="loginpanel">
			<div class="mainnews">
				<div class="inlogin">
					<div class="namelogin">
						<span><i class="fa fa-user"></i> Foydalanuvchining ma'lumotlarini o'zgartirish</span>
					</div>
					<div class="inputinfo">
						<form action="{{ route('getUserchange') }}" method="post">
							{!! csrf_field() !!}
							<div class="log">
								<input type="text" name="firstname" placeholder="Ismingiz" value="{{ $user->firstname }}" />
								<i class="fa fa-user"></i>
							</div>
							<div class="log">
								<input type="text" name="lastname" placeholder="Familyangiz" value="{{ $user->lastname }}" />
								<i class="fa fa-user"></i>
							</div>
							<div class="log">
								
								<input type="text" name="tel_number" placeholder=""  style="width: 50%;font-size: 15px;" value="{{ $user->tel_number }}" />
								<i class="fa fa-phone"></i>
							</div>
							<div class="log">
								<input type="text" name="login" placeholder="Loginingiz"value="{{ $user->login }}" />
								<i class="fa fa-sign-in"></i>
							</div>
							<input type="text" name="id" value="{{ $user->id }}" style="display: none;">
							<div class="log">
								<select class="textage" name="country">
									<option>
										Tashkent shahar
									</option>
									<option>
										Toshkent viloyat
									</option>
									<option>
										Qoraqalpog'iston
									</option>
									<option>
										Xorazm	
									</option>
									<option>
										Surxondaryo 
									</option>
									<option>
										Qashqadaryo
									</option>
									<option>
										Navoiy	
									</option>
									<option>
										Buxoro	
									</option>
									<option>
										Samarqand	
									</option>

									<option>
										Jizzax 	
									</option>
									<option>
										Sirdaryo	
									</option>
									<option>
										Farg'ona	
									</option>
									<option>
										Namangan
									</option>
									<option>
										Andijon	
									</option>
								</select>
							</div>
							<div class="log">
								<select class="textage" name="type">
									<option>
										Maktab o'quvchisi
									</option>
									<option>
										Maktabni tugallagan
									</option>
								</select>
							</div>
							<div class="log">
								<input type="password" name="password" placeholder="Parolni kiriting "  />
								<i class="fa fa-unlock-alt" ></i>
							</div>
							<div class="log">
								<select class="textage" name="usertype">
									@foreach($usertypes as $usertype)
									<option>
										{{ $usertype->type }}
									</option>
									@endforeach
								</select>
							</div>
							<div class="postlogin tg-bannercontent1">
								<button class="tg-btn1" type="submit">
									<span>
										<i class="fa fa-sign-in"></i>
										O'zgartirish
									</span>
								</button>
							</div>
							<div class="forgetpassw">	
								@if(Session::has('fail'))
								<h6 style="color: red;font-size: 13px;"> {{ Session::get('fail') }} </h6>
								@endif									
		 					</div>
		 				</form>
		 			</div>
		 		</div>
		 	</div>
		 </div>
		</div>
	</div>
