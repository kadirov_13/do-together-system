@extends('layout.main')

@section('header')
@include('includes.header1')
@section('header')

@section('content')

<div class="mainnews" >
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Xabar qo'shish</p>
		</div>
		<div class="search">
			<div class="inputb">

			</div>
		</div>
	</div>
	<div class="table" style="background-color: #fff">
		<form action="{{ route('addNew') }}" method="post" enctype="multipart/form-data">
			<div class="addtema">
				<span>Fan : </span>
				<select class="subjecttest" name="subject">
					@foreach($subjects as $subject)
						<option>{{ $subject->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="addtema">
				<span>Mavzu : </span>
				<input type="text" name="theme">
			</div>

			<div class="form-group">
				<span>Xabar:</span>	
				<textarea class="form-control" name="info" id="input" rows="10"></textarea>
			</div>

			<div class="addtema">
				<span>Muallif : </span>
				<input type="text" name="autor">
			</div>
			<div class="addtema">
				<span>Rasm : </span>
				<input type="file" name="image">
			</div>
			{!! csrf_field() !!}
			<div class="exam3">
				<button type="submit" style="max-height: 60px;">
					<i class="fa fa-plus"></i>Qo'shish
				</button>
			</div>
		</form>
	</div>
</div>

@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')

@section('script')
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=5x3ngooqcfd46b27y4kxt79abx577l8m03dus6pqstdlktdz
"></script>
    <script type="text/javascript">
      tinymce.init({
      selector: 'textarea',
      theme: 'modern',
      plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
      toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      powerpaste_word_import: 'merge',
     });
    </script>
@endsection('script)