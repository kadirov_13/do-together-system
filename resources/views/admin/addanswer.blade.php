@extends('layout.main')

@section('header')
@include('includes.header1')
@section('header')

@section('content')

<div class="mainnews" >
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Test uchun javob qo'shish</p>
		</div>
		<div class="search">
			<div class="inputb">

			</div>
		</div>
	</div>
	<div class="table" style="background-color: #fff">
		<form action="{{ route('addAnswer') }}" method="post" enctype="multipart/form-data">
			<div class="addtema">
				<span>Nomi : </span>
				<input type="text" name="test_id" value="{{$test->name_test}}">
			</div>
			<div class="addtema">
				<input type="file" name="answer">
			</div>
			{!! csrf_field() !!}
			<div class="exam3">
				<button type="submit" style="max-height: 60px;">
					<i class="fa fa-plus"></i>Qo'shish
				</button>
			</div>
		</form>
	</div>
</div>

@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
