  @extends('layout.adminmain')

  @section('header')
  @include('includes.admin.header')
  @endsection('header')


  @section('content')
  <div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Foydalanuvchilar</h3> </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Jadvallar</a></li>
                    <li class="breadcrumb-item active">Foydalanuvchilar</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                      @if(Session::has('fail'))
                      <h6 style="color: green;">{{ Session::get('fail') }}</h6>
                      @endif
                      <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="table_id" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Foydalanuvchi</th>
                                        <th>Shahri</th>
                                        <th>Login</th>
                                        <th>Toifasi</th>
                                        <th>Telefon raqam</th>
                                        <th>Darajasi</th>
                                        <th>Amal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>
                                            <a href="{{ route('actionUserinfo',['id'=> $user->id,'n'=>0]) }}">{{ $user->lastname }} {{ $user->firstname }}
                                        </a>
                                        </td>
                                        <td>{{ $user->country }}</td>
                                        <td>{{ $user->login }}</td>
                                        <td>{{ $user->type }}</td>
                                        <td>{{ $user->tel_number }}</td>
                                        <td>{{ $user->usertype->type }}</td>

                                        <td>
                                            <a href="" title="delete" data-toggle="modal" data-target="#myModal{{ $user->id }}"> <i class="fa fa-trash icon11"></i></a>
                                            <a href="{{ route('actionChangeuser',['id'=>$user->id]) }}" title="change"> <i class="fa fa-edit icon12"></i></a>
                                            <a href="" title="delete" data-toggle="modal" data-target="#myModal1{{ $user->id }}"> <i class="fa fa-plus-circle icon13"></i></a>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="myModal{{ $user->id }}" role="dialog">
                                        <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                            <div class="modal-header"> 
                                              <h4 class="modal-title">Ma'lumotni o'chirish</h4>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          </div>
                                          <div class="modal-body">
                                            <p class="delete">Siz haqiqatdan ham foydalanuvchi  <b>{{ $user->lastname }} {{ $user->firstname }} </b>ni  o'chirmoqchimisiz !!!</p>

                                        </div>
                                        <div class="modal-footer">
                                            <a href="{{ route('deleteuser',['id'=>$user->id]) }}" class="btn btn-primary" >Ha</a>
                                            <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">yo'q</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal1{{ $user->id }}" role="dialog">
                                <div class="modal-dialog modal-lg">
                                  <form action="{{ route('changeusertype') }}" method="post">
                                  <div class="modal-content">
                                    <div class="modal-header"> 
                                      <h4 class="modal-title">Foydalanuvchi huquqi</h4>
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  </div>
                                  <div class="modal-body">
                                    <p class="delete">Foydalanuvchi  <b>{{ $user->lastname }} {{ $user->firstname }} </b>ni  darajasini o'zgartirish !!!</p>
                                    <div class="log">
                                        <select class="textage" name="usertype">
                                            @foreach($usertypes as $usertype)
                                            <option>
                                                {{ $usertype->type }}
                                            </option>
                                            @endforeach
                                        </select>
                                        <input type="text" name="id" value="{{ $user->id }}" style="display: none;">
                                    </div>
                                    {!! csrf_field() !!}
                                    <p class="delete">O'zgarishni tasdiqlaysimi !!!</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" >Ha</button>
                                    <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">yo'q</button>

                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    @endforeach

                </tbody>
            </table>

        </div>
    </div>
</div>
</div>
</div>
</div>

@endsection('content')
@section('script')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#table_id').DataTable();
    } );
</script>
@endsection('script')