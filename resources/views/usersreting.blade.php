@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')



@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Qatnashchilar retingi</p>
		</div>
		<div class="search">
			<div class="inputb">
			<form action="{{ route('searchUser') }}" method="post">		
				<div class="inputborder">
					<input type="text" name="searchuser" placeholder="Qidirayotganingizni kiriting....">
					<i class="fa fa-search"></i>
				</div>
				{!! csrf_field() !!}
				<button type="submit">Qidirish</button>
			</form>
			</div>
		</div>
	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> <i class="fa fa-trophy"></i> № </th>
					<th> <i class="fa fa-user"></i> Foydalanuvchi </th>
					<th><i class="fa fa-globe"></i>Viloyati</th>
					<th><i class="fa fa-envelope"></i> Jami jo'natmalar</th>
					<th><i class="fa fa-check-circle"></i>  Ball </th>
					<th><i class="fa fa-calendar"></i> Oxirgi tashrif</th>
				</tr>
			</thead>
			<tbody>
				<?php $reting = ($ufbs->currentPage()-1) * $ufbs->perPage() + 1;  ?>

				@if(Auth::check())
				@foreach($ufbs as $ufb)
				@if(Auth::user()->id == $ufb->user->id)
					<tr style="background-color: #B4EDFA;">		
						<td> {{ $reting++ }}</td>
						<td>
							<a href="{{ route('actionUserinfo',['id'=> $ufb->user->id,'reting'=>$reting-1]) }}">{!! $ufb->user->lastname !!}
								{!! $ufb->user->firstname !!}
							</a>
						</td>
						<td>{{ $ufb->user->country }}</td>
						<td>{{ $ufb->count_answer}}</td>
						<td>{!! $ufb->ball !!}</td>
						<td>online</td>
					</tr>
				@else
					<tr>							
						<td> {{ $reting++ }}</td>
						<td>
							<a href="{{ route('actionUserinfo',['id'=> $ufb->user->id,'n'=>$reting-1]) }}">{!! $ufb->user->lastname !!}
								{!! $ufb->user->firstname !!}
							</a>
						</td>
						<td>{{ $ufb->user->country }}</td>
						<td>{{ $ufb->count_answer}}</td>
						<td>{!! $ufb->ball !!}</td>
						@if($ufb->user->isOnline())
						<td style="color:green">online </td>
						@else
						<td>{!! $ufb->lasttime !!}</td>
						@endif
					</tr>
				@endif
				@endforeach
				@else
				@foreach($ufbs as $ufb)
				<tr>
					<td> {{ $reting++ }}</td>
					<td>
						<a href="{{ route('actionUserinfo',['id'=> $ufb->user->id]) }}">{!! $ufb->user->lastname !!}
							{!! $ufb->user->firstname !!}
						</a>
					</td>
					<td>{{ $ufb->user->country }}</td>
					<td>{{ $ufb->count_answer}}</td>
					<td>{!! $ufb->ball !!}</td>
					<td>{!! $ufb->lasttime !!}</td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
	</div>
	<div class="moreinfonumber">

			{!! $ufbs->links() !!}

	</div>
</div>
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
