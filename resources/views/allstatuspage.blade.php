@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Barcha test savollariga yo'llangan javoblar jadvali</p>
		</div>

	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> <i class="fa fa-list-ol"></i>№</th>
					<th> <i class="fa fa-user"></i> Foydalanuchi </th>
					<th><i class="fa fa-edit"></i> Test nomi</th>
					<th><i class="fa fa-calendar"></i> Vaqt</th>
					<th><i class="fa fa-bell"></i> Natija</th>

					<!-- <th><i class="fa fa-calendar"></i> Oxirgi jo'natma</th> -->
				</tr>
			</thead>
			<tbody>
				<?php $number = ($utfs->currentPage()-1) * $utfs->perPage() + 1;  ?>
				@foreach($utfs as $utf)						   
				<tr>
					<td>{!! $number++ !!}</td>
					<td><a href="{{ route('actionUserinfo',['id'=>$utf->user->id]) }}">{!! $utf->user->firstname !!}  {!! $utf->user->lastname !!}</a></td>
					<td><a href="{{ route('actionTestexam',['id'=>$utf->test->id]) }}">{!! $utf->test->name_test !!}</a></td>
					<td>{{ $utf->gettime }}</td>
					@if($utf->resoult==1)
					<td class="true">To'g'ri</td>
					@elseif($utf->resoult==0)
					<td class="false">Noto'g'ri</td>
					@else
					<td class="viewtest">Ko'rilgan</td>

					@endif
				</tr>						    
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="moreinfonumber">
		{!! $utfs->links() !!}
	</div>
</div>	
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')