@extends('layout.main')

@section('header')
@include('includes.header1')
@section('header')

@section('content')

<div class="mainnews" >
	<div class="searchcontent">
		<div class="retingsearch text-center">
			<p><i class="fa fa-users"></i> Bellashuvga so'rov jo'natish</p>
		</div>
		<div class="search">
			<div class="inputb">
                
			</div>
		</div>
	</div>
	<div class="table" style="background-color: #fff">
		<form action="{{ route('addCompetition')}}" method="post" enctype="multipart/form-data">
			<div class="addtema" style="display: none;">
				<span>User 1 : </span>
				<input type="text" name="user1" value=" {!! $user1->id !!}">
			</div>

			<div class="addtema" style="display: none;">
				<span>User 2 : </span>
				<input type="text" name="user2" value=" {!! $user2->id !!}">
			</div>
			<div class="addtema">
				<span>User 1 : </span>
				<input type="text" name="ok" value=" {!! $user1->lastname !!}  {!! $user1->firstname !!}">
			</div>

			<div class="addtema">
				<span>User 2 : </span>
				<input type="text" name="okkk" value=" {!! $user2->lastname !!} {!! $user2->firstname !!}">
			</div>
			<div class="addtema">
				<span>Fan : </span>
				<select class="subjecttest" name="subject">
					@foreach($subjects as $subject)
						<option>{{ $subject->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="addtema">
				<span>Testlar soni : </span>
				<input type="text" name="count_tests" id="inwidh">
			</div>
			<div class="addtema">
				<span>Ball tikish : </span>
				<select class="subjecttest" name="ball">
						<option>shunchaki</option>
						<option>8</option>
						<option>12</option>
						<option>16</option>
						<option>20</option>
				</select>
			</div>
			
			<div class="addtema">
				<span>Davomiyligi : </span>
				<input type="time" name="length" id="inwidh">
			</div>
			{!! csrf_field() !!}
			<div class="exam3">
				<button type="submit" style="max-height: 60px;">
					<i class="fa fa-plus"></i>Boshlash
				</button>
			</div>
		</form>
	</div>
</div>

@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
