
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Online testing</title>

    <link href="{{ URL::to('src') }}/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::to('src/assets/custom/') }}/css/flexslider.css" type="text/css" media="screen">      
    <link rel="stylesheet" href="{{ URL::to('src/assets/custom/') }}/css/parallax.css" type="text/css">
    <link rel="stylesheet" href="{{ URL::to('src/') }}/assets/font-awesome-4.0.3/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('src/')}}/css/stylemain1.css">
    <link href="{{ URL::to('src/assets/custom/') }}/css/business-plate.css" rel="stylesheet">
</head>
<!-- NAVBAR
    ================================================== -->
    <body>
        <div class="top">
            <div class="container">
                <div class="row-fluid">
                    <ul class="phone-mail">
                        <li><i class="fa fa-phone"></i><span>+998(91) 260-56-37</span></li>
                        <li><i class="fa fa-envelope"></i><span>kadirov_ogabek@mail.ru</span></li>
                        <li><i class="fa fa-telegram-plane"></i><span>@kadirov_ogabek</span></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <!-- topHeaderSection -->   
        <div class="topHeaderSection">      
            <div class="header">            
                <div class="container">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo1" href="{{route('actionIndex')}}"><img src="{{ URL::to('src/')}}/img/logo/innew.png"/></a>
                </div>
                <div class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    
                  </ul>
                  <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#">Asosiy oyna</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Fanlar <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        @foreach($subjects as $subject)
                        <li><a href="{{ route('getsubject',['id' => $subject->id]) }}" >{!! $subject->name !!}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li><a href="#about">Tizim haqida</a></li>
                <li><a href="#about">Dasturchi</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
</div>

<!-- bannerSection -->
<div class="bannerSection">
    <div class="container">
    <div class="slider-inner">
        <div id="da-slider" class="da-slider">
            <div class="da-slide">
                <h2>
                    <i>Online</i>
                    <i>Testlar </i> 
                    <br> 
                    <i>siz istagan fanlardan</i>
                </h2>
                <p>
                    <i>1.Bilimingizni sinab ko'rishni istaysizmi.</i>
                    <br>
                    <i>2.Siz istagan paytda,istalgan fandan,istalgan mavzularda online test yechish imkoniyatiga egasiz.</i>
                    <br>
                    <i>3.Istalgan qurilma orqali orqali (telefon,kompyuter).</i>
                </p>
                <div class="da-img firstimg"><img src="{{ URL::to('src/')}}/img/bir.png" alt="" /></div>
            </div>
            
            <div class="da-slide">
                <h2>
                    <i>Bilimingizni sinang</i> 
                    <br> 
                    <i>ballarga ega bo'ling</i>
                    <br>
                    <i>Reytingda o'z o'rningizni egallang.</i> 

                </h2>
                <p>
                    <i>1.Butun O'zbekiston bo'ylab qatnashchilar reytingi.</i> 
                    <br> 
                    <i>2.Real vaqtda o'zaro bir biringiz bilan bellashish imkoniyati. </i> 
                    <br> 
                    <i>3.O'z guruhini tuzish va guruhlar orasidagi bellashuvlarda qatnashish.</i>
                </p>
                <div class="da-img"><img src="{{ URL::to('src/')}}/img/ss1.png" alt="" /></div>
            </div>
            
            
            
            <div class="da-slide">
                <h2>
                    <i>Online olimpiadalar</i> 
                    <br> 
                    <i>Bilimlar to'qnashuvi</i>
                </h2>
                <p>
                    <i>1.Real vaqt davomida bellashuvlar.</i> 
                    <br> 
                    <i>2.Olimpiadada g'olib chiqing va qo'shimcha ballarga ega boling. </i>
                </p>
                <div class="da-img ">
                    <img src="{{ URL::to('src/')}}/img/okko.png"/>
                </div>
            </div>
            <nav class="da-arrows">
                <span class="da-arrows-prev"></span>
                <span class="da-arrows-next"></span>        
            </nav>
        </div><!--/da-slider-->
    </div><!--/slider-->
    <!--=== End Slider ===-->

</div>
</div>
<!-- highlightSection -->
<div class="highlightSection">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
               <h2>Dasturning maqsadi</h2>
               <em> yurtimizda bilimga chanqoq o'g'il va qizlarni birlashtirish.</em><br><br>
               Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'stlar bilan bellashish imkoniyatini yarating.Shu bilan bir qatorda o'zingiz ham do'stlaringiz bilan bellashing va ular bilan bilim almashing.Bilimingizga yarasha ballarga ega bo'ling va Respublika bo'yicha reytingda o'z o'rningizni egallang. 
           </p>
       </div>
       <div class="col-md-4 align-right"> 
        <h4>Dasturning ishlashi haqidagi to'liq ma'lumotlar. </h4>
        <div class="tg-bannercontent">
            <a class="tg-btn" href="javascript:void(0);(0);"><span>Ko'rish...</span></a>
        </div>
    </p>
</div>
</div>
</div>
</div>
<!-- imkoniyatlar -->
<div class="services">
    <div class="container">
        <div class="signsubjects">
            <p><span>Imkoniyatlar</span></p>
        </div>
        <div class="row">
            <div class="col-md-3 newhead">
                <img src="{{ URL::to('src/img') }}/tuit1.jpg" class="" title="project one">              
                <p class="newsinfo">Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'stlar bilan bellashish imkoniyatini yarating.</p>
                <div class="tg-bannercontent pull-right">
                    <a class="tg-btn" href="javascript:void(0);(0);">
                        <span>Ko'proq...</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 newhead">
                <img src="{{ URL::to('src/img') }}/tuit1.jpg" class="" title="project one">
                
                
                <p class="newsinfo">Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'stlar bilan bellashish imkoniyatini yarating.</p>
                <div class="tg-bannercontent pull-right">
                    <a class="tg-btn" href="javascript:void(0);(0);">
                        <span>Ko'proq...</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 newhead">
                <img src="{{ URL::to('src/img') }}//tuit1.jpg" class="" title="project one">
                
                
                <p class="newsinfo">Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'slar bilan bellashish imkoniyatini yarating.</p>
                <div class="tg-bannercontent pull-right">
                    <a class="tg-btn" href="javascript:void(0);(0);">
                        <span>Ko'proq...</span>
                    </a>
                </div>
            </div> 
            <div class="col-md-3 newhead">
                <img src="{{ URL::to('src/img') }}/tuit1.jpg" class="" title="project one">
                
                
                <p class="newsinfo">Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'slar bilan bellashish imkoniyatini yarating.</p>
                <div class="tg-bannercontent pull-right">
                    <a class="tg-btn" href="javascript:void(0);(0);">
                        <span>Ko'proq...</span>
                    </a>
                </div>
            </div>
        </div> 
    </div>
</div>

<!-- endimkoniyatlar -->
<!-- bodySection -->
<div class="serviceBlock">
    <div class="container ">
        <div class="signsubjects">
            <p><img src="{{ URL::to('src/img/subjects/') }}/fan7.png"><span>Fanlar</span></p>
        </div>
        <div class="container">
            <div class="row ">
                <div class="col-md-4 col-xs-6">
                    <div class="clearfix">
                        <a href="{{  route('getsubject',['id' => 1])  }}">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="{{ URL::to('src/img/subjects/') }}/infor1.png">
                                    <!-- <i class="fa fa-compress"></i> -->
                                </div>
                                <div class="col-md-9">
                                    <div class="desc">
                                        <h4>Informatika</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-xs-6">
                    <div class="clearfix">
                       <a href="{{  route('getsubject',['id' => 2])  }}" >
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ URL::to('src/img/subjects/') }}/math1.png">
                                <!-- <i class="fa fa-compress"></i> -->
                            </div>
                            <div class="col-md-9">
                                <div class="desc">
                                    <h4>Matematika</h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-xs-6">
                <div class="clearfix">
                    <a href="{{  route('getsubject',['id' => 3])  }}">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ URL::to('src/img/subjects/') }}/fizika.png">
                                <!-- <i class="fa fa-compress"></i> -->
                            </div>
                            <div class="col-md-9">
                                <div class="desc">
                                    <h4>FIzika</h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </a>
        </div>
        <div class="col-md-4 md-offset-4 col-xs-6 ">
            <div class="clearfix">
                <a href="{{  route('getsubject',['id' => 4])  }}">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{ URL::to('src/img/subjects/') }}/kimyo.png">
                            <!-- <i class="fa fa-compress"></i> -->
                        </div>
                        <div class="col-md-9">
                            <div class="desc">
                                <h4>Kimyo</h4>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </a>
    </div>
    <div class="col-md-4 col-xs-6">
        <div class="clearfix">
            <a href="{{  route('getsubject',['id' => 5])  }}">
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{ URL::to('src/img/subjects/') }}/biology.png">
                        <!-- <i class="fa fa-compress"></i> -->
                    </div>
                    <div class="col-md-9">
                        <div class="desc">
                            <h4>Biologiya</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </a>
</div>
<div class="col-md-4 col-xs-6">
    <div class="clearfix">
        <a href="{{  route('getsubject',['id' => 6])  }}">
            <div class="row">
                <div class="col-md-3">
                    <img src="{{ URL::to('src/img/subjects/') }}/tarix2.png">
                    <!-- <i class="fa fa-compress"></i> -->
                </div>
                <div class="col-md-9">
                    <div class="desc">
                        <h4>Tarix</h4>
                    </div>
                </div>
            </div>
        </a>
    </div>
</a>
</div>
<div class="col-md-4 md-offset-4 col-xs-6">
    <div class="clearfix">
        <a href="{{  route('getsubject',['id' => 7])  }}">
            <div class="row">
                <div class="col-md-3">
                    <img src="{{ URL::to('src/img/subjects/') }}/adab.png">
                    <!-- <i class="fa fa-compress"></i> -->
                </div>
                <div class="col-md-9">
                    <div class="desc">
                        <h4>Ona tili va Adabiyot</h4>
                    </div>
                </div>
            </div>
        </a>
    </div>
</a>
</div>

<div class="col-md-4 col-xs-6">
    <div class="clearfix">
        <a href="{{  route('getsubject',['id' => 8])  }}">
            <div class="row">
                <div class="col-md-3">
                    <img src="{{ URL::to('src/img/subjects/') }}/en.png">
                    <!-- <i class="fa fa-compress"></i> -->
                </div>
                <div class="col-md-9">
                    <div class="desc">
                        <h4>Ingliz tili</h4>
                    </div>
                </div>
            </div>
        </a>
    </div>
</a>
</div>
<div class="col-md-4 col-xs-6">
    <div class="clearfix">
        <a href="{{  route('getsubject',['id' => 9])  }}">
            <div class="row">
                <div class="col-md-3">
                    <img src="{{ URL::to('src/img/subjects/') }}/ru1.png">
                    <!-- <i class="fa fa-compress"></i> -->
                </div>
                <div class="col-md-9">
                    <div class="desc">
                        <h4>Rus tili</h4>
                    </div>
                </div>
            </div>
        </a>
    </div>
</a>
</div>  
</div>
</div>  
</div>
</div>
<hr>


<div class="services">
    <div class="container">
        <div class="signsubjects">
            <p><img src="{{ URL::to('src/img/subjects/') }}/new2.png"><span>Yangiliklar</span></p>
        </div>
        <div class="row">
            <div class="col-md-3 newhead">
                <img src="{{ URL::to('src/img') }}/tuit1.jpg" class="" title="project one">              
                <p class="newsinfo">Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'stlar bilan bellashish imkoniyatini yarating.</p>
                <div class="tg-bannercontent pull-right">
                    <a class="tg-btn" href="javascript:void(0);(0);">
                        <span>Ko'proq...</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 newhead">
                <img src="{{ URL::to('src/img') }}/tuit1.jpg" class="" title="project one">
                
                
                <p class="newsinfo">Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'stlar bilan bellashish imkoniyatini yarating.</p>
                <div class="tg-bannercontent pull-right">
                    <a class="tg-btn" href="javascript:void(0);(0);">
                        <span>Ko'proq...</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 newhead">
                <img src="{{ URL::to('src/img') }}//tuit1.jpg" class="" title="project one">
                
                
                <p class="newsinfo">Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'slar bilan bellashish imkoniyatini yarating.</p>
                <div class="tg-bannercontent pull-right">
                    <a class="tg-btn" href="javascript:void(0);(0);">
                        <span>Ko'proq...</span>
                    </a>
                </div>
            </div> 
            <div class="col-md-3 newhead">
                <img src="{{ URL::to('src/img') }}/tuit1.jpg" class="" title="project one">
                
                
                <p class="newsinfo">Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'slar bilan bellashish imkoniyatini yarating.</p>
                <div class="tg-bannercontent pull-right">
                    <a class="tg-btn" href="javascript:void(0);(0);">
                        <span>Ko'proq...</span>
                    </a>
                </div>
            </div>
        </div> 
    </div>
</div>


<div class="testimonails">
    <div class="container">
        <blockquote class="">
            <p>Respublika bo'yicha yagona reyting,online olimpiadalar,do'stlarni bellashuvga chorlash,guruhlar tashkil qilish va bir vaqtning o'zida ular bilan bellashish imkoniyatlari yaratildi.</p>
            <small>Kadirov Og'abek</small>
        </blockquote>       
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4"> 
            <h3 class="title">Sayt imkoniyatlari</h3>
            <p>
                Saytdagi foydalanuvchi ro'yxatdan o'tgandan keyin  unda hamma fanlardan test yechish va ball yig'ib respublika reyting bellashuvida qatnashish imkoniyati bo'ladi.  
            </p>
            <ul>
                <li>Har bir fanga alohida respublika reytingi.</li>
                <li>Online olimpiadalarda qatnashish.</li>
                <li>Do'stini bellashuvga chorlash</li>
                <li>Guruhlar tashkil qilish va unga odam to'plash</li>
                <li>O'z guruhi bilan boshqa guruhni bellashuvga chorlash</li>
            </ul>
            <div id="mappanda">
                
            </div>
        </div>
        <div class="col-md-8">
            <div class="projectList">
                <h3 class="title">G'oliblar</h3>
                <div class="media">
                  <a class="pull-left" href="#">
                    <img src="{{ URL::to('src/assets/custom/') }}/img/project1.jpg" class="projectImg" title="project one">
                </a>
                <div class="media-body">
                    <h4 class="media-heading"><a href="#">Matematika</a></h4>
                    <p>
                        Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'slar bilan bellashish imkoniyatini yarating.Shu bilan bir qatorda o'zingiz ham do'stlaringiz bilan bellashing va ular bilan bilim almashing.Bilimingizga 
                    </p>
                    <div class="tg-bannercontent pull-right">
                        <a class="tg-btn" href="javascript:void(0);(0);">
                            <span>Ko'proq...</span>
                        </a>
                    </div>
                </div>
            </div>
            
            <div class="media">
              <a class="pull-left" href="#">
                <img src="{{ URL::to('src/assets/custom/') }}/img/project2.jpg" class="projectImg" title="project one">
            </a>
            <div class="media-body">
                <h4 class="media-heading"><a href="#">Kimyo</a></h4>
                <p>
                    Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'slar bilan bellashish imkoniyatini yarating.Shu bilan bir qatorda o'zingiz ham do'stlaringiz bilan bellashing va ular bilan bilim almashing.Bilimingizga 
                </p>
                <div class="tg-bannercontent pull-right">
                    <a class="tg-btn" href="javascript:void(0);(0);">
                        <span>Ko'proq...</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="media">
          <a class="pull-left" href="#">
            <img src="{{ URL::to('src/assets/custom/') }}/img/project1.jpg" class="projectImg" title="project one">
        </a>
        <div class="media-body">
            <h4 class="media-heading"><a href="#">English</a></h4>
            <p>
                Ushbu tizim haqida do'stlaringizga xabar bering va ularga yangi do'slar bilan bellashish imkoniyatini yarating.Shu bilan bir qatorda o'zingiz ham do'stlaringiz bilan bellashing va ular bilan bilim almashing.Bilimingizga 
            </p>
            <div class="tg-bannercontent pull-right">
                <a class="tg-btn" href="javascript:void(0);(0);">
                    <span>Ko'proq...</span>
                </a>
            </div>
        </div>
    </div>      
    
</div>
</div>
</div>
</div>
</div>
</div>
<!-- clientSection -->
<div class="container">
    <h3 class="title">Homiylar</h3>
    <div class="clientSection">
        <div class="row">                   
            <div class="col-md-4">
                <a href="#"><img class="mxwimg" alt="" src="{{ URL::to('src')}}/img/logo/uzb.jpg"></a>
            </div>
            <div class="col-md-4">
                <a href="#"><img alt="" src="{{ URL::to('src')}}/img/logo/pandasoft.jpg"></a>
            </div>
            <div class="col-md-4">
                <a href="#"><img alt="" src="{{ URL::to('src')}}/img/logo/pandalab.jpg"></a>
            </div>
        </div>
    </div>
</div>



<!-- footerTopSection -->
<div class="footerTopSection">
    <div class="container">
        <div class="row">
          
          <div class="col-md-3">
            <h3>Biz haqimizda</h3>
            <p>
                Toshkent axborot texnologiyalari universiteti<br>
                <strong> Pandalab </strong> dasturchilar jamoasi.
            </p>
            

<!--                <div>
                    <form class="form-inline" role="form">
                      <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                      </div>
                      <button type="submit" class="btn btn-brand">Subscribe</button>
                    </form>
                </div> -->
                
            </div>
            <div class="col-md-3">
                <h3>Pandalab haqida</h3>
                <p>Agar siz pandalab haqida bilishni istasangiz
                   <a href="pandalab.uz">pandalab.uz</a>
                   shaxsiy sayti orqali buni amalga oshirishingiz mumkin.
               </p>
           </div>
           <div class="col-md-3">
            <h3>PandaSoft</h3>
            <p>
                <strong>PandaSoft XK</strong><br>
                Toshkent shahar furqat ko'chasi 7 uy<br>
                <!-- United Kingdon<br> -->
                Tel : +99 8(91) 260-56-37<br>
                IN : PandaSoft Company<br>
                F : PandaSoft Group<br>
                S :www.pandasoft.uz
            </p>
            <!-- <h3>Xi</h3> -->
        </div>
        <div class="col-md-3">
            <h3>Pandasoft hizmatlar</h3>
            <p>
                Web ilovalar yaratish<br>
                Mobil ilovalar yaratish<br>
                Kompyuter uchun dasturiy ta'minotlar
            </p>
        </div>
    </div>
</div>
</div>
<!-- footerBottomSection -->    
<div class="footerBottomSection">
    <div class="container">
        &copy; Ishga tushgan vaqt 2018 y 1- iyun. <a href="#">Jamoa PandaLab</a> | <a href="#">By PandaSoft</a> 
<!--            <div class="pull-right">
             <a href="index.html">
                <img src="img/logo1.png"  />
             </a>
         </div> -->
     </div>
 </div>
 
 <!-- JS Global Compulsory -->           
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/jquery-1.8.2.min.js"></script>
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/modernizr.custom.js"></script>     
 <script type="text/javascript" src="{{ URL::to('src/')}}/assets/bootstrap/js/bootstrap.min.js"></script> 
 
 <!-- JS Implementing Plugins -->           
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/jquery.flexslider-min.js"></script>
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/modernizr.js"></script>
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/jquery.cslider.js"></script> 
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/back-to-top.js"></script>
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/jquery.sticky.js"></script>

 <!-- JS Page Level -->           
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/app.js"></script>
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/index.js"></script>


 <script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        App.initSliders();
        Index.initParallaxSlider();
    });
</script>
<!-- PandaSoft Company map -->
<script type="text/javascript">
   var map;
   function initMap() {
       map = new google.maps.Map(document.getElementById('mappanda'), {
           center: {lat: 41.311081, lng: 69.240562},
           zoom: 13
       });
       var marker = new google.maps.Marker({
        position:{lat: 41.311081, lng: 69.240562},
        map: map,
        title: 'PandaSoft'
    }); 
   }
</script>
<!-- End pandasoft company map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApQzNe3WFd4wWK3sH8W9XW9V6CHHLaUN0&callback=initMap"
async defer>

</script>

</body>
</html>