@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')


<div class="mainnews">
	<div class="searchcontent">
		<div class="onlinetestname">
			<span><i class="fa fa-globe"></i> Bellashuv Natijalari </span>
		</div>
		<div class="nameexam">
			<div class="row">
				<div class="col-md-6">
					<p>
						<a href="{{ route('actionCompetitionTest',['message'=>$message]) }}">
							<i class="fa fa-list-ol"></i> 
							<span>Testlar</span>
						</a>
					</p>
				</div>
				<div class="col-md-6">
				</div>
			</div>
			<div class="timeolimpiada">
				<p>
					<span>
						<strong id="begin">Boshlandi </strong>
					</span>
					<span>
						Qolgan vaqt :
						<strong id="demo"><i></i></strong>
					</span>
				</p>
			</div>
		</div>

	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> <i class="fa fa-list-ol"></i>O'rni</th>
					<th> <i class="fa fa-user"></i> Foydalanuvchi </th>
					<th><i class="fa fa-check-circle"></i> Ishlanganlar </th>
					<th><i class="fa fa-book"></i>Ball</th>
				</tr>
			</thead>
			<tbody>	
				<?php $number = 1; ?>

			<?php 
				$ball = 0;
				$truetests='';
				$utfcs = App\Utfc::where('user_id',$user_1->id)->where('competition_id',$id)->get();
				for($i=0;$i<count($utfcs);$i++)
				{
					if ($utfcs[$i]->resoult==1) {
						$ball = $ball+$utfcs[$i]->ball;
						$truetests.=$utfcs[$i]->test->name_test.',';
					}	
				}

			 ?>					   
				<tr>
					<td>{{ $number++ }}</td>
					<td>
						<a href="#">{!! $user_1->lastname !!}
								{!! $user_1->firstname !!}
						</a>
					</td>
					<td>{{ $truetests }}</td>
					<td>{{ $ball }}</td>
				</tr>	
			<?php 
				$ball = 0;
				$truetests='';
				$utfcs = App\utfc::where('user_id',$user_2->id)->where('competition_id',$id)->get();
				for($i=0;$i<count($utfcs);$i++)
				{
					if ($utfcs[$i]->resoult==1) {
						$ball = $ball+$utfcs[$i]->ball;
						$truetests.=$utfcs[$i]->test->name_test.',';
					}	
				}

			 ?>					   
				<tr>
					<td>{{ $number++ }}</td>
					<td>
						<a href="#">{!! $user_2->lastname !!}
								{!! $user_2->firstname !!}
						</a>
					</td>
					<td>{{ $truetests }}</td>
					<td>{{ $ball }}</td>
				</tr>					    
			</tbody>
		</table>
	</div>
	<div class="moreinfonumber">
		
	</div>
	<?php $messagetime = App\Message::where('id',$message)->first()->created_at;
		 $messageendtime = App\Message::where('id',$message)->first()->competition->length;
		 $timenow = Carbon\Carbon::now();

			// $seconds = $messagetime->h*3600 + $messagetime->i*60 + $messagetime->s;
		?>
		<p style="display: none;" id="begintime">{{ $messagetime }}</p>
		<p style="display: none;" id="endtime">{{ $messageendtime }}</p>
		<p style="display: none;" id="timenow">{{ $timenow }}</p>
</div>
@endsection('content')
@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
@section('script')

<script type="text/javascript">

var myVar = setInterval(myTimer, 1000);
	var n = 0;
     $time = document.getElementById('demo');
// var sec = new Date();

// var hh = sec.getHours();
// var mm = sec.getMinutes();
// var ss = sec.getSeconds() 

// var timenow = hh*3600 + mm*60 + ss;
var time2 = document.getElementById('timenow').innerHTML;
// alert(time2);
var thisDateT2 = time2.substr(0, 10) + "T" + time2.substr(11, 8);
var jDate2 = new Date(thisDateT2);
var timenow = jDate2.getHours()*3600+jDate2.getMinutes()*60+jDate2.getSeconds();

var time1 = document.getElementById('begintime').innerHTML;
var thisDateT = time1.substr(0, 10) + "T" + time1.substr(11, 8);
var jDate = new Date(thisDateT);
var timebegin = jDate.getHours()*3600+jDate.getMinutes()*60+jDate.getSeconds();

var endtime = document.getElementById('endtime').innerHTML;
var thisDateT1 =  "2018-06-26T" + endtime.substr(0, 8);
var jDate1 = new Date(thisDateT1);
var endtime= jDate1.getHours()*3600+jDate1.getMinutes()*60+jDate1.getSeconds();
var time_left = endtime - (timenow-timebegin);
// alert(timebegin);
	function myTimer() {
		if(time_left<=0){
			$time.innerHTML="00:00:00 vaqt tugadi !!!";
			document.getElementById('begin').style.display = 'none';

    	
    	}
    	else{
    		time_left--;
        $time.innerHTML = Math.floor(time_left/3600) +':'+Math.floor((time_left%3600)/60)+':'+time_left%60;
    	}
    }

</script>
@endsection('script')
	