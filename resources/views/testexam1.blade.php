@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')

@section('content')
@if($true==1)
<div class="mainnews">
	<div class="newscontent">
		<div class="nameexam">

			<div class="row">

				<div class="col-md-6">

					<p>
						<strong>
							<i>Test Nomi:</i>
						</strong>
						<span> {!! $utf->test->name_test !!}</span>
					</p>
					<p>
						<strong>
							<i>Urinishlar soni:</i>
						</strong>

						@if($utf->count_answer==0)
						<span> Javob yo'llamagansiz</span>
						@else
						<span> {!! $utf->count_answer !!}  ta javob yo'llagansiz</span>
						<p>
							<strong>
								<i>Natija:</i>
								@if($utf->resoult==1)
								<span class="true">To'g'ri</span>
								@else
								<span class="false">Noto'g'ri</span>
								@endif
							</strong>
						</p>
						@endif

					</p>	
				</div>
				<div class="col-md-6">
					<p>
						<strong>
							<i>Asl ball :</i>
						</strong>
						<span> {!! $utf->test->ball !!} ball</span>
					</p>
					<p>
						<strong>
							<i>Joriy ball:</i>
						</strong>
						<span>{{$utf->ball}} ball</span>
					</p>	
				</div>
			</div>
		</div>
		<div class="headexam">
			<span>Savolning berilishi</span>
		</div>
		<div class="exam">
			<span>
				{!! $utf->test->exam !!}
			</span>
		</div>
		<div class="answer">
			<div class="headexam">
				<span>Variyantlar</span>
			</div>

			<div class="variants" style="border-left: 2px solid #3864ba;
			padding-left: 1em;	">
			<p>A )
				<span>{!! $utf->test->a_answer !!}</span>
			</p>
			<p>B )
				<span>{!! $utf->test->b_answer !!}</span>
			</p>
			<p>C ) 
				<span>{!! $utf->test->c_answer !!}</span>
			</p>
			<p>D ) 
				<span>{!! $utf->test->d_answer !!}</span>
			</p>
			@if(Session::has('fail'))
			<h6 style="color: red;font-size: 13px;"> {{ Session::get('fail') }} </h6>
			@endif
			@if(Auth::user()->usertype->type == 'admin')
			<div class="login">
				<a href="{{ route('addAnswerpanel',['id'=>$utf->test->id]) }}"> 
					<i class="fa fa-share-square"></i>Javob qo'shish
				</a>
			</div>
			@endif	
		</div>
		<form action="{{ route('getanswer') }}" method="post">
			<div class="row">
				<div class="col-md-3">

				</div>
				<div class="col-md-6">
					<div class="getanswer">
						<span>Javob : </span>
						<select name="answer">
							<option>A</option>
							<option>B</option>
							<option>C</option>
							<option>D</option>
						</select>
					</div>
					{!! csrf_field() !!}
					<input type="text" name="test" value="{{ $utf->test->id }}" style="display: none;">
				</div>
				<div class="col-md-3">
					<div class="login">
						<button type="submit"> 
							<i class="fa fa-share-square"></i>Jo'natish
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
@else

<div class="mainnews">
	<div class="newscontent">
		<div class="nameexam">

			<div class="row">

				<div class="col-md-6">

					<p>
						<strong>
							<i>Test Nomi:</i>
						</strong>
						<span> {!! $test->name_test !!}</span>
					</p>
					<p>
						<strong>
							<i>Urinishlar soni:</i>
						</strong>


						<span> Javob yo'llamagansiz</span>


					</p>	
				</div>
				<div class="col-md-6">
					<p>
						<strong>
							<i>Asl ball :</i>
						</strong>
						<span> {!! $test->ball !!} ball</span>
					</p>
					<p>
						<strong>
							<i>Joriy ball:</i>
						</strong>
						<span>{{ $test->ball }} ball</span>
					</p>	
				</div>
			</div>
		</div>
		<div class="headexam">
			<span>Savolning berilishi</span>
		</div>
		<div class="exam">
			<span>
				{!! $test->exam !!}
			</span>
		</div>
		<div class="answer">
			<div class="headexam">
				<span>Variyantlar</span>
			</div>

			<div class="variants" style="border-left: 2px solid #3864ba;
			padding-left: 1em;	">
			<p>A )
				<span>{!! $test->a_answer !!}</span>
			</p>
			<p>B )
				<span>{!! $test->b_answer !!}</span>
			</p>
			<p>C ) 
				<span>{!! $test->c_answer !!}</span>
			</p>
			<p>D ) 
				<span>{!! $test->d_answer !!}</span>
			</p>	
		</div>
	</div>
	<p>
		<span style="color:#3864ba"><i class="fa fa-question-circle"></i>
			Siz javob yo'llash uchun ro'yhatdan o'tishingiz va o'z profilingizga kirishingiz kerak !!! 
<br>
			<a class="tg-btn" href="{{ route('registration') }}">
				<span>
					<i class="fa fa-registered"></i> Ro'yxatdan o'tish
				</span>
			</a>
							
		</span>
	</p>
</div>
</div>
@endif

@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
