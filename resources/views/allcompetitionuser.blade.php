@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Barcha o'tkazilgan bellashuvlar ro'yxati</p>
			<p>To'liq ma'lumot uchun ixtiyoriy foydalanuvchi ismini bosing</p>
		</div>

	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<!-- <th> <i class="fa fa-list-ol"></i>№</th> -->
					<th> <i class="fa fa-user"></i> 1 - foydalanuvchi</th>
					<th><i class="fa fa-edit"></i> ball</th>
					<th><i class="fa fa-pluc"></i></th>
					<th><i class="fa fa-edit"></i> ball</th>
					<th><i class="fa fa-user"></i>2 - foydalanuvchi</th>
					<!-- <th><i class="fa fa-book"></i></th> -->
				</tr>
			</thead>
			<tbody>
				<?php $time = Carbon\Carbon::now()->toDateTimeString();
				?>
				@foreach($messages as $message)
<!-- -->
				<?php 
			
				$user_1 = App\User::where('id',$message->user_1)->first();
				$user_2 = App\User::where('id',$message->user_2)->first();
				$ball_1 = 0;
				// $truetests='';
				$utfcs = App\Utfc::where('user_id',$user_1->id)->where('competition_id',$message->competition_id)->get();
				for($i=0;$i<count($utfcs);$i++)
				{
					if ($utfcs[$i]->resoult==1) {
						$ball_1 = $ball_1+$utfcs[$i]->ball;
						// $truetests.=$utfcs[$i]->test->name_test.',';
					}	
				}
				$ball_2 = 0;
				// $truetests='';
				$utfcs = App\Utfc::where('user_id',$user_2->id)->where('competition_id',$message->competition_id)->get();
				for($i=0;$i<count($utfcs);$i++)
				{
					if ($utfcs[$i]->resoult==1) {
						$ball_2 = $ball_2+$utfcs[$i]->ball;
						// $truetests.=$utfcs[$i]->test->name_test.',';
					}	
				}


				?>
				
				@if($ball_1>$ball_2)					   
				<tr >
					<td class="win">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $user_1->lastname!!}  {!! $user_1->firstname!!}
						</a>
					</td>
					<td class="win">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $ball_1!!}</a>
					</td>
					<td >
						-
					</td>
					<td class="notwin">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $ball_2!!}</a>
					</td>
					<td class="notwin">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $user_2->lastname!!}  {!! $user_2->firstname!!}
						</a>
					</td>
			</tr>	
			@elseif($ball_1<$ball_2)		
			<tr>
			
					<td class="notwin">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $user_1->lastname!!}  {!! $user_1->firstname!!}</a>
					</td>
					<td class="notwin">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $ball_1!!}</a>
					</td>
					<td>
						-
					</td>
					<td class="win">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $ball_2!!}</a>
					</td>
					<td class="win">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $user_2->lastname!!}  {!! $user_2->firstname!!}</a>
					</td>
			</tr>
			@else
			<tr>
					<td class="eque"> 
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $user_1->lastname!!}  {!! $user_1->firstname!!}</a>
					</td>
					<td class="eque">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $ball_1!!}</a>
					</td>
					<td>
						-
					</td>
					<td class="eque">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $ball_2!!}</a>
					</td>
					<td class="eque">
						<a href="{{ route('actionCompetitionresoult',['message'=>$message->id]) }}">{!! $user_2->lastname!!}  {!! $user_2->firstname!!}</a>
					</td>
			</tr>
			@endif
			@endforeach
		</tbody>
	</table>
</div>
<div class="moreinfonumber">
	<div class="moreinfonumber">
		<div class="retingsearch">
			<p>
				<span class="infocolor" style="background: green" ></span> - Yutgan foydalanuvchi
			</p>
			<p >
				<span class="infocolor " style="background: red" ></span> - Yutqazgan foydalanuchi
			</p>
			<p >
				<span class="infocolor " style="background: #3864ba" ></span> - Durrang
			</p>
		</div>
	</div>
	{!! $messages->links() !!}
</div>
</div>	
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')