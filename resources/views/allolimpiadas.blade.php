@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Barcha o'tkazilgan olimpiadalar ro'yxati</p>
		</div>

	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<!-- <th> <i class="fa fa-list-ol"></i>№</th> -->
					<th> <i class="fa fa-user"></i> Olimpiada nomi </th>
					<th><i class="fa fa-edit"></i> Fan </th>
					<th><i class="fa fa-calendar"></i> Boshlanish vaqti</th>
					<th><i class="fa fa-bell"></i>Davomiyligi</th>
				</tr>
			</thead>
			<tbody>
				<?php $number = 1; $time =Carbon\Carbon::now()->toDateTimeString();
				    $number = ($olimps->currentPage()-1) * $olimps->perPage() + 1;
							?>
				
				@foreach($olimps as $olimpiada)		
				<?php $olimptime=date("Y-m-d H:i:s",strtotime($olimpiada->begin_time) - strtotime('00:00') + strtotime($olimpiada->length));?>
				@if($olimpiada->subject_id!=0)
				@if($olimptime<$time)					   
				<tr style="background-color: #FFC7D2">
					<td>
						<a href="{{ route('actionOlimpiada',['id'=>$olimpiada->id]) }}">{!! $olimpiada->name !!}</a>
					</td>
					<td>
						{!! $olimpiada->subject->name!!}</a>
					</td>
					<td>
						{!! $olimpiada->begin_time!!}</a>
					</td>
					<td>
						{!! $olimpiada->length!!}</a>
					</td>
				</tr>	
				@elseif($olimpiada->begin_time<$time and $olimptime>$time) 		
				<tr style="background-color: #BCFCCB">
					<td>
						<a href="{{ route('actionOlimpiada',['id'=>$olimpiada->id])}}">{!! $olimpiada->name !!}</a>
					</td>
					<td>
						{!! $olimpiada->subject->name!!}</a>
					</td>
					<td>
						{!! $olimpiada->begin_time!!}</a>
					</td>
					<td>
						{!! $olimpiada->length!!}</a>
					</td>
				</tr>	
				@else
				<tr style="background-color: #A8E2DB">
					<td>
						<a href="{{route('actionOlimpiada',['id'=>$olimpiada->id])}}">{!! $olimpiada->name !!}</a>
					</td>
					<td>
						{!! $olimpiada->subject->name!!}</a>
					</td>
					<td>
						{!! $olimpiada->begin_time!!}</a>
					</td>
					<td>
						{!! $olimpiada->length!!}</a>
					</td>
				</tr>    
				@endif
				@endif
				@endforeach
			</tbody>
		</table>
	<div class="moreinfonumber">
		{!! $olimps->links() !!}
	</div>
	</div>
	<div class="moreinfonumber">
		<div class="moreinfonumber">
			<div class="retingsearch">
				<p>
					<span class="infocolor mycolor1" ></span> - Yakunlangan olimpiadalar
				</p>
				<p >
					<span class="infocolor mycolor2" ></span> - Hozir bo'layotgan olimpiadalar
				</p>
				<p >
					<span class="infocolor mycolor3" ></span> - Endi bo'ladigan olimpiadalar
				</p>
			</div>
		</div>
	</div>
</div>	
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')