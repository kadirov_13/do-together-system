@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')
@if(is_null(Auth::user()))
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Ma'vjud guruhlar bo'limi.</p>
			<p>Siz bu ma'lumotlar bo'limini ko'rish uchun ro'yxatdan o'tishingiz va o'z profilingizga kirishingiz lozim !!!</p>
		</div>

	</div>
</div>
@else
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Ma'vjud guruhlar bo'limi.</p>
			<p>Guruhlar yoki maktablarning ma'lumotlarini ko'rish uchun nomini ustiga bosing.</p>
			@if(Session::has('fail'))
				<h6 style="color: red;font-size: 13px;" id="error"> {{ Session::get('fail') }} </h6>
								@endif	
		</div>

	</div>
	<div class="table whitecolor">
		<div class="row">
			<div class="col-md-4">
				<div class="schoolgroup mycolor2" onclick="tab(1)">
						<div class="row">
						<div class="col-md-12"><i class="fa fa-users"></i></div>
						<div class="col-md-12">Guruhlar</div>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="schoolgroup mycolor1" onclick="tab(2)">
						<div class="row">
						<div class="col-md-12"><i class="fa fa-building"></i></div>
						<div class="col-md-12">Maktablar</div>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="schoolgroup mycolor3" onclick="tab(3)">
						<div class="row">
						<div class="col-md-12"><i class="fa fa-book"></i></div>
						<div class="col-md-12">O'quv markazlari</div>

					</div>
				</div>
			</div>
		</div>
	<div class="moreinfonumber"  id="tab1" style="display: none">
			<div class="searchcontent">
		<div class="retingsearch">
			<p>To'p guruhlar 10 taligi.>>>>
				<a href="" style="text-decoration: underline;"> <b>Barcha guruhlarni ko'rish !!! </b></a>

			</p>
		</div>

	</div>
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th><i class=""></i> № </th>
					<th><i class="fa fa-globe"></i> Guruh nomi </th>
					<th><i class="fa fa-trophy"></i> Reyting </th>
					<th><i class="fa fa-users"></i>Azolar soni </th>
					<th><i class="fa fa-user"></i>Admin</th>
					<th><i class="fa fa-plus-square "></i>Azo bo'lish</th>


				</tr>
			</thead>
			<tbody >
				<?php 
				$ngp=1;

				 ?>
				@foreach($gpgroups as $gpgroup)
				<?php

					$users_count = count(App\User::where('gpgroup_id',$gpgroup->id)->get()); 
				?>
					<tr>							
						<td>{{ $ngp++ }}</td>
						<td>
							<a href="{{ route('actionGroupinfo',['id'=>$gpgroup->id]) }}">{{ $gpgroup->name}}
							</a>
						</td>
						<td>{{$gpgroup->reyting}} </td>
						<td>{{ $users_count }}</td>
						<td><a href="{{ route('actionUserinfo',['id'=>$gpgroup->admin_id,'reting'=>0]) }}">admin </a></td>

						<td>
							<div class="tg-bannercontent">
								<a class="tg-btn" href="{{ route('getmessagegroup',['id'=>$gpgroup->id,'type'=>1]) }}">
									<span>
										Azo bo'lish
									</span>
								</a>
							</div>

						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

	</div>
	<div class="moreinfonumber" id="tab2" style="display: none">

		<table class="table table-striped table-bordered table-hover " >
			<thead>
				<tr>
					<th><i class=""></i> № </th>
					<th><i class="fa fa-globe"></i> Maktab nomi </th>
					<th><i class=""></i> Viloyati </th>
					<th><i class="fa fa-trophy"></i> Reyting </th>
					<th><i class="fa fa-users"></i>Azolar soni </th>
					<th><i class="fa fa-calendar"></i>Ma'lumotni ko'rish</th>
					<th><i class="fa fa-plus-square "></i>Azo bo'lish</th>


				</tr>
			</thead>
			<tbody >

					<tr>							
						<td> 1</td>
						<td>
							<a href="">122-maktab 
							</a>
						</td>
						<td>Toshkent shahri </td>

						<td>232 </td>
						<td>22 </td>
						<td>
							<div class="tg-bannercontent">
								<a class="tg-btn" href="">
									<span>
										Ma'lumot
									</span>
								</a>
							</div>

						</td>
						<td>
							<div class="tg-bannercontent">
								<a class="tg-btn" href="">
									<span>
										Azo bo'lish
									</span>
								</a>
							</div>

						</td>
					</tr>

			</tbody>
		</table>
	</div>
	<div class="moreinfonumber"  id="tab3" style="display: none">

		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th><i class=""></i> № </th>
					<th><i class="fa fa-globe"></i>Markaz nomi  </th>
					<th><i class=""></i>Viloyati  </th>
					<th><i class="fa fa-trophy"></i> Reyting </th>
					<th><i class="fa fa-users"></i>Azolar soni </th>
					<th><i class="fa fa-calendar"></i>Ma'lumotni ko'rish</th>
					<th><i class="fa fa-plus-square "></i>Azo bo'lish</th>


				</tr>
			</thead>
			<tbody >

					<tr>							
						<td> 1</td>
						<td>
							<a href="">Domination
							</a>
						</td>
						<td>Toshkent shahri </td>

						<td>232 </td>
						<td>22 </td>
						<td>
							<div class="tg-bannercontent">
								<a class="tg-btn" href="">
									<span>
										Ma'lumot
									</span>
								</a>
							</div>

						</td>
						<td>
							<div class="tg-bannercontent">
								<a class="tg-btn" href="">
									<span>
										Azo bo'lish
									</span>
								</a>
							</div>

						</td>
					</tr>

			</tbody>
		</table>
	</div>
	</div>
	<div class="moreinfonumber whitecolor">
			<div class="retingsearch">
				<p>
					<span class="infocolor mycolor2" ></span> -Guruhlar.  Shunchaki birga ishlash uchun do'stlar tomonidan tashkil qilingan guruhlardan biriga azo bo'lishingiz mumkin !!!
				</p>
				<p>
					<span class="infocolor mycolor1" ></span> - Maktablar.Bu bo'limdan o'z maktabingizni tanlashingiz va unga azo bo'lishingiz mumkin !!!
				</p>
				<p >
					<span class="infocolor mycolor3" ></span> - O'quv markazlari.O'zingiz tayyorlangan yoki endi tayyorlanayotgan o'quv markaziga azo bo'ling !!!
				</p>
			</div>
	</div>
</div>	
@endif
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
@section('script')
<script type="text/javascript">
	
	function tab($a){
	if($a==1)
	{
		document.getElementById('tab1').style.display = 'block';
		document.getElementById('tab2').style.display = 'none';
		document.getElementById('tab3').style.display = 'none';
		

	}
	else{
		if ($a==2) {

		document.getElementById('tab1').style.display = 'none';
		document.getElementById('tab2').style.display = 'block';
		document.getElementById('tab3').style.display = 'none';

		}
		else{
		document.getElementById('tab1').style.display = 'none	';
		document.getElementById('tab2').style.display = 'none';
		document.getElementById('tab3').style.display = 'block';

		}

	}
	}
</script>

@endsection('script')