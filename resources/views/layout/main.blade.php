<!DOCTYPE html>
<html>
<head>
	<title>onlinetest</title>
	<link rel="stylesheet" type="text/css" href="{{ URL::to('src')}}/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ URL::to('src')}}/css/stylemain1.css">
	<link rel="stylesheet" type="text/css" href="{{ URL::to('src/')}}/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="{{ URL::to('src')}}/css/nav.css">
	<link rel="stylesheet" type="text/css" href="{{ URL::to('src/css/menyu.css') }}">

</head>
<body>

@yield('header')
<div class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				@yield('content')
			</div>
			<div class="col-md-3">
				@yield('menyu')
			</div>
		</div>
	</div>
</div>
@yield('footer')



<script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/jquery-1.8.2.min.js"></script>
 <script type="text/javascript" src="{{ URL::to('src/assets/custom/') }}/js/modernizr.custom.js"></script>     
 <script type="text/javascript" src="{{ URL::to('src/')}}/assets/bootstrap/js/bootstrap.min.js"></script> 
@yield('script')
<!-- <script type="text/javascript" scr="{{ URL::to('src/tinymce/js/tinymce/tinymce.min.js') }}"></script> -->
</body>
