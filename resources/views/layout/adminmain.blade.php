<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <title>Osys.uz</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::to('src/admin/css/') }}/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->

    <link href="{{ URL::to('src/admin/css/') }}/lib/calendar2/semantic.ui.min.css" rel="stylesheet">
    <link href="{{ URL::to('src/admin/css/') }}/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
    <link href="{{ URL::to('src/admin/css/') }}/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="{{ URL::to('src/admin/css/') }}/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="{{ URL::to('src/admin/css/') }}/helper.css" rel="stylesheet">
    <link href="{{ URL::to('src/admin/css/') }}/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <script src="{{ URL::to('src/admin/js/') }}/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <!-- <script src="{{ URL::to('src/admin/js/') }}/lib/bootstrap/js/popper.min.js"></script> -->
    <script src="{{ URL::to('src/admin/js/') }}/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ URL::to('src/admin/js/') }}/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="{{ URL::to('src/admin/js/') }}/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="{{ URL::to('src/admin/js/') }}/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
    <script src="{{ URL::to('src/admin/js/') }}/lib/morris-chart/raphael-min.js"></script>
    <script src="{{ URL::to('src/admin/js/') }}/lib/morris-chart/morris.js"></script>
    <script src="{{ URL::to('src/admin/js/') }}/lib/morris-chart/dashboard1-init.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script src="{{ URL::to('src/admin/js/') }}/scripts.js"></script>
</head>

<body class="fix-header fix-sidebar">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- Main wrapper  -->
        <div id="main-wrapper">
            @yield('header')
            @yield('content')
            @yield('footer')
            @yield('script')
        </div>
    </body>
    </html>