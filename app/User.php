<?php

namespace App;
use Cache;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function ufb()
    {
        return $this->belongsTo('App\Ufb');
    }
    public function image()
    {
        return $this->hasOne('App\Image','id','image_id');        
    }
    public function utf()
    {
        return $this->hasMany('App\Utf');
    }
    public function isOnline()
    {
        return Cache::has('user-is-online-'.$this->id);
    }
     public function competiton()
    {
        return $this->belongsTo('App\Competition');
    }
    public function usertype()
    {
        return $this->hasOne('App\Usertype','id','usertype_id');
    }
    public function gpgroup()
    {
        return $this->hasOne('App\Gpgroup','id','gpgroup_id');
    }
    public function gpsch()
    {
        return $this->hasOne('App\Gpsch','id','gpsch_id');
    }public function gplcenter()
    {
        return $this->hasOne('App\Gplcenter','id','gplcenter_id');
    }
}
