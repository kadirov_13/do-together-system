<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Subject;
use App\Test;
use App\Groupmessage;
Route::get('/',[

	'uses'=>'ActionController@actionIndex',
	'as'=>'actionIndex'
]);

Route::get('/testlab',function(){
$users = App\User::all();
dd($users[1]->usertype->type);
});

Route::get('/subjectnews',[

	'uses'=>'ActionController@actionSubject',
	'as'=>'actionSubject'

]);

Route::get('/getsubject/{id?}',[
	
	'uses' => 'SubjectController@setSubject',
	'as' => 'getsubject'
]);

Route::get('/tests',[
	
	'uses' => 'ActionController@actionTest',
	'as' => 'actionTest'
]);

Route::get('/testexam/{id?}',[
'uses'=>'ActionController@actionTestexam',
'as'=>'actionTestexam'

]);

Route::post('/getUser',[
	
	'uses' => 'UserController@setuser',
	'as' => 'getUser'	
]);

Route::get('/logout',[
	
	'uses' => 'UserController@logout',
	'as' => 'logout'	
]);

Route::post('/enter',[
	
	'uses' => 'UserController@enter',
	'as' => 'enter'	
]);


Route::get('/reting',[
	
	'uses' => 'ActionController@actionReting',
	'as' => 'actionReting'	
]);



Route::get('/status',[
	'uses'=>'ActionController@actionStatus',
	'as'=>'actionStatus'

]);
Route::get('/userinfo/{id?}/{reting?}',[
	'uses'=>'ActionController@actionUserinfo',
	'as'=>'actionUserinfo'

]);
Route::get('/userstatus/{id?}',[
	'uses'=>'ActionController@actionUserstatus',
	'as'=>'actionUserstatus'

]);
Route::get('/onlinetest',function(){
	return view('onlinetest');
})->name('onlinetest');


Route::get('/login',function(){
	return view('loginpage');
})->name('login');

Route::get('/registration',function(){
	return view('registration');
})->name('registration');


Route::post('getanswer',[

	'uses'=>'TestController@setanswer',
	'as'=>'getanswer'

]);

Route::get('/olimpiada/{id?}',[
	'uses'=>'ActionController@actionOlimpiada',
	'as'=>'actionOlimpiada'
]);

Route::get('/allolimpiadas',[
	'uses'=>'ActionController@actionAllolimpiadas',
	'as'=>'actionAllolimpiadas'
]);


Route::post('/setuserimage',[
	'uses'=>'UserController@userimage',
	'as'=>'userimage'
]);


Route::get('/truetests/{id?}/{n?}',[
	'uses'=>'UserController@truetests',
	'as'=>'truetests'
]);
Route::post('/searchtest',[

	'uses'=>'TestController@searchTest',
	'as'=>'searchTest'
]);

Route::post('/searchuser',[

	'uses'=>'UserController@searchUser',
	'as'=>'searchUser'
]);

Route::get('/bookssubject',[

	'uses'=>'ActionController@allbooks',
	'as'=>'actionBooks'
]);

Route::get('/answers',[
	'uses'=>'ActionController@allanswers',
	'as'=>'actionAnswer'

]);
Route::get('/getTest',[
	'uses'=>'TestController@gettests',
	'as'=>'getTest'

]);
Route::get('/addolimpiadapanel',[
	'uses'=>'ActionController@addOlimpiadapanel',
	'as'=>'actionOlimpiadapanel'

]);
Route::post('/addOlimpiada',[

	'uses'=>'OlimpiadaController@addOlimpiada',
	'as'=>'addOlimpiada'
]);

Route::get('/resoultolimpiada/{id?}',[

	'uses'=>'ActionController@Resoultolimpiada',
	'as'=>'actionResoultolimp'
]);
Route::get('/usertouserpanel/{id?}',[
	'uses'=>'UserController@usertouserpanel',
	'as'=>'ActionUsertouser'
]);
Route::get('/allonlineusers',[
	'uses'=>'ActionController@OnlineUsers',
	'as'=>'ActionOnlineUsers'

]);
Route::post('/addcompetition',[

	'uses'=>'CompetitionController@addCompetition',
	'as'=>'addCompetition'

]);
Route::get('/waitpanel/{message?}',[
	'uses'=>'ActionController@Waitpanel',
	'as'=>'actionWaitpanel'

]);
Route::get('/allConditions/{id?}',[
	'uses'=>'ActionController@Messages',
	'as'=>'actionMessages'
]);
Route::get('/actionCompetitionanswer/{anwer?}/{message?}',[
	'uses'=>'ActionController@Competitionanswer',
	'as'=>'actionCompetitionanswer'
]);
Route::get('/actionCompetitionanswerfalse/{message?}',[
	'uses'=>'ActionController@Competitionanswerfalse',
	'as'=>'actionCompetitionanswerfalse'
]);
Route::get('/actionCompetitionanswertrue/{message?}',[
	'uses'=>'ActionController@Competitionanswertrue',
	'as'=>'actionCompetitionanswertrue'
]);
Route::get('/actionCompetitiontestget/{message?}',[
	'uses'=>'ActionController@Competitiontestget',
	'as'=>'actionCompetitionTestget'
]);
Route::get('/actionCompetitiontest/{message?}',[
	'uses'=>'ActionController@Competitiontest',
	'as'=>'actionCompetitionTest'
]);
Route::get('/actionCompetitionexam/{id?}/{comp?}',[
	'uses'=>'ActionController@Competitionexam',
	'as'=>'actionCompetationexam'
]);
Route::post('/getanswercomp',[
   'uses'=>'TestController@getanswercomp',
   'as'=>'getanswercomp',
]);
Route::get('/compstatus/{comp?}/{message?}',[
	'uses'=>'ActionController@CompStatus',
	'as'=>'actionCompetitionuserstatus'
]);
Route::get('/compresoult/{message?}',[
	'uses'=>'CompetitionController@Compresoult',
	'as'=>'actionCompetitionresoult'
]);
Route::get('/getmg',[
	'uses'=>'CompetitionController@setmg',
	'as'=>'getmg'
]);
Route::get('/allCompetition/{id?}',[
	'uses'=>'ActionController@allCompetition',
	'as'=>'actionallCompetitionuser'
]);

Route::post('/getGroup_gr',[
   'uses'=>'GroupsController@getGroup_gr',
   'as'=>'getGroup_gr',
]);

Route::get('/login',[
	'uses'=>'AdminactionController@login',
	'as'=>'bestlogin'
]);
Route::get('/grouplist',[
	'uses'=>'ActionController@grouplist',
	'as'=>'actionGroupslist'
]);

Route::get('/actionGpgroup',[
	'uses'=>'ActionController@actionGpgroup',
	'as'=>'actionGpgroup'
]);

Route::get('/actionGroupinfo/{id?}',[
	'uses'=>'ActionController@actionGroupinfo',
	'as'=>'actionGroupinfo'
]);

Route::get('actionGroupmessages/{id?}',[
	'uses'=>'ActionController@actionGroupmessages',
	'as'=>'actionGroupmessages'
]);
Route::get('getmessagegroup/{id?}/{type?}',[
	'uses'=>'GroupsController@getmessagegroup',
	'as'=>'getmessagegroup'
]);
Route::get('actionGroupmessageanswer/{answer?}/{id?}',[
	'uses'=>'GroupsController@actionGroupmessageanswer',
	'as'=>'actionGroupmessageanswer'
]);
Route::get('usergroupyes/{id?}/{gp?}',function($id=null,$gp=null){
$message = Groupmessage::where('id',$id)->first();
if (!is_null($message)) {
	$message->delete();
}
// dd($gp);
	return redirect()->route('actionGroupinfo',[
		'id'=>$gp,
	]);
})->name('usergroupyes');
Route::group(['middleware' => 'admin'],function()
{
Route::get('/adminusers',[
	'uses'=>'AdminactionController@users',
	'as'=>'AdminUsers'
]);

Route::get('/adminalltests',[
	'uses'=>'AdminactionController@adminalltests',
	'as'=>'adminalltests'
]);
Route::get('/adminallnews',[
	'uses'=>'AdminactionController@adminallnews',
	'as'=>'adminallnews'
]);

Route::get('/admin',[
	'uses'=>'AdminactionController@index',
	'as'=>'AdminIndex'

]);


Route::post('/addbook',[

	'uses'=>'SubjectController@addBook',
	'as'=>'addBook'
]);

Route::get('/addbook',[
	'uses'=>'ActionController@addbookpanel',
	'as'=>'addbookpanel'
]);
Route::get('/addnew',[
	'uses'=>'ActionController@addnewpanel',
	'as'=>'addnewpanel'

]);

Route::get('/addanswer/{id?}',[
	'uses'=>'ActionController@addAnswerpanel',
	'as'=>'addAnswerpanel'
]);
Route::post('/addanswer',[

	'uses'=>'TestController@addanswer',
	'as'=>'addAnswer'
]);

Route::post('/addNew',[

	'uses'=>'SubjectController@addNew',
	'as'=>'addNew'
]);
Route::get('/new/{id?}',[
	'uses'=>'ActionController@viewNews',
	'as'=>'actionViewnew'

]);
Route::get('/addtest',[
	'uses'=>'ActionController@addtestpanel',
	'as'=>'actionAddtest'
]);

Route::post('/edittest',[
	'uses'=>'TestController@edittest',
	'as'=>'edittest'
]);

Route::post('/addtesting',[
	'uses'=>'TestController@addtest',
	'as'=>'addtest'
]);

Route::get('/deletetest/{id}',[
	'uses'=>'AdminactionController@deletetest',
	'as'=>'deletetest'
]);
Route::get('/changetest/{id}',[
	'uses'=>'AdminactionController@changetest',
	'as'=>'adminchangetest'
]); 

Route::get('/changenew/{id}',[
	'uses'=>'AdminactionController@changenewpanel',
	'as'=>'actionchangenew'
]);

Route::get('/deletenewid/{id}',[
	'uses'=>'AdminactionController@deletenewid',
	'as'=>'deletenewid'
]);
Route::post('/changenew',[
	'uses'=>'AdminactionController@Changenewid',
	'as'=>'Changenewid'
]);


Route::get('/deleteuser/{id}',[
	
	'uses' => 'AdminactionController@deleteuser',
	'as' => 'deleteuser'	
]);
Route::get('/actionChangeuser{id}',[
	'uses'=>'AdminactionController@Changeuser',
	'as' => 'actionChangeuser'

]);

Route::post('/changeusertype',[
	'uses'=>'AdminactionController@changeusertype',
	'as' => 'changeusertype'

]);
Route::post('/changeuser',[
	'uses'=>'AdminactionController@getUserchange',
	'as'=>'getUserchange'
]);

});