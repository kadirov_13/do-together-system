<?php
namespace App\Http\Controllers;

Use App\Subject;
use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
use App\Book;
use Illuminate\Support\Facades\Input;
use Carbon\carbon;
use App\Newmessage;
Use App\Image;

/**
* 
*/
class SubjectController extends Controller
{

	public function setSubject($id=null)
	{
		// dd($id);
		Session::put('subject_id',$id,4600);
       // dd(Session::get('subject_id'));
		return redirect()->route('actionSubject');
	}
	public function addBook(Request $request)
	{
		$book = new Book();
		// dd(Subject::where('name',$request['subject'])->first()->id);
		// dd($request['subject']);
		
		$book->subject_id=Subject::where('name',$request['subject'])->first()->id;
		$book->name = $request['name'];
		if($request->hasFile('book'))
		{	
			$location = 'src/books';
			$file = Input::file('book');
 			$format = $file->getClientOriginalExtension();
 			$timestamp = str_replace([' ',':'], '-',Carbon::now()->toDateTimeString());
			$name = $timestamp.'.'.$format;
			$path = public_path() . '/' . $location . '/';
			$file->move($path,$name);
			$path = $location.'/'.$name;
			$book->path = $path;
		}
		// dd($book);
		$book->save();
			return redirect()->route('actionBooks');
	}

	public function addNew(Request $request)
	{
		// dd('salom');
		$subject = Subject::where('name',$request['subject'])->first();
		$new = new Newmessage();
		$new->theme = $request['theme'];
		$new->info = $request['info'];
		$new->subject_id =$subject->id;
		$new->count_views = 0;
		$new->autor = $request['autor'];
		$new->time = Carbon::now()->toDateTimeString();
		if($request->hasFile('image'))
		{
			$file = Input::file('image');

			$image_id = ImageController::setimage($file,0);

			$new->image_id = $image_id; 

		}
		else $new->image_id = 21;
		// dd($new);
		$new->save();
		return redirect()->route('actionSubject');
	}
}


// 15-may kuni <b><i>matematika</i></b> fani boyicha fan <b>olimpiadasi<b> bolib otadi.Agar sizda unda qatnashish istagi tug'ilsa saytdan royhardan o'ting va olimpiada vaqti o'z bilimlaringizni sinab ko'ring.olimpiada goliblari alohida taqdirlanadi ularga retingda o'z orinlarini egallashlari uchun <b>ballar</b> taqdim etiladi

