<?php  

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Image;
use Illuminate\Support\Facades\Input;
use App\Requests;

/**
* 
*/
class ImageController extends Controller
{
	
	static function setimage($file,$a)
	{
		if($a==1)
		{
			$image = new Image();
		}
		else
		{
			$image = Image::where('id',$a)->first(); 
		}
		$timestamp = str_replace([' ',':'], '-',Carbon::now()->toDateTimeString());
		$name = $timestamp.'.jpg';
		$path = public_path().'/src/imgusers';
		$file->move($path,$name);
		$path = '/src/imgusers/'.$name;
		// dd($path);
		$image->path = $path;
		$image->save();
		return ($image->id);
	}
}


