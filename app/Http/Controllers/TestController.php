<?php

namespace App\Http\Controllers;

use App\Test;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Subject;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Ufb;
use App\Utfc;
use App\Utf;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Answer;
use App\Olimpiada;
use App\Competition;
use App\Message;

/**
* 
*/
class TestController extends Controller
{
	public function addtest(Request $request)
	{
		$this->validate($request,[
			'a_var' => 'required|max:100',
			'b_var' => 'required|max:100',
			'c_var' => 'required|max:100',
			'b_var' => 'required|max:100',
		]);
		// dd($request['subject']);
		$subject = Subject::where('name',$request['subject'])->first();
		$test = new Test();
		$test->subject_id = $subject->id;
		$test->theme = $request['theme'];
		$test->exam = $request['exam'];
		$test->a_answer = $request['a_var'];
		$test->b_answer = $request['b_var'];
		$test->c_answer = $request['c_var'];
		$test->d_answer = $request['d_var'];
		$test->utf_id = 0;
		$test->name_test = $request['name_test'];
		$test->true_answer = $request['true_answer'];
		$test->ball = $request['ball'];
		// dd(Olimpiada::where('name',$request['olimpiada'])->first()->id);
		$test->olimpiada_id = Olimpiada::where('name',$request['olimpiada'])->first()->id;
		$test->true_answers = 0;
		$test->have_answer = false;
		$test->save();

		$tests = Test::where('subject_id',$subject->id)->paginate(13);

		return redirect()->route('adminalltests')->with(['fail'=>'Ma\'lumot \qo\'shildi !!! ']);

	}
	public function edittest(Request $request)
	{
		$this->validate($request,[
			'a_var' => 'required|max:100',
			'b_var' => 'required|max:100',
			'c_var' => 'required|max:100',
			'b_var' => 'required|max:100',
		]);
		// dd($request['subject']);
		$subject = Subject::where('name',$request['subject'])->first();
		$test = Test::where('id',$request['test_id'])->first();
		$test->subject_id = $subject->id;
		$test->theme = $request['theme'];
		$test->exam = $request['exam'];
		$test->a_answer = $request['a_var'];
		$test->b_answer = $request['b_var'];
		$test->c_answer = $request['c_var'];
		$test->d_answer = $request['d_var'];
		$test->utf_id = 0;
		$test->name_test = $request['name_test'];
		$test->true_answer = $request['true_answer'];
		$test->ball = $request['ball'];
		// dd(Olimpiada::where('name',$request['olimpiada'])->first()->id);
		$test->olimpiada_id = Olimpiada::where('name',$request['olimpiada'])->first()->id;
		$test->true_answers = 0;
		$test->have_answer = false;
		$test->save();

		$tests = Test::where('subject_id',$subject->id)->paginate(13);

		return redirect()->route('adminalltests')->with(['fail'=>'Ma\'lumot o\'zgartirildi !!!']);

	}

	public function setanswer(Request $request)
	{

		$user = Auth::user();
		// dd($user->id);
			$subject_id = 0;
		if(is_null(Session::get('subject_id')))
			{
				$subject_id = 1;
			}
		else
		{
			$subject_id = Session::get('subject_id');
		}
			// dd($subject_id);
		$ufb = Ufb::where('user_id',Auth::user()->id)->where('subject_id',$subject_id)->first();
			 // dd($ufb);
		$test = Test::where('id',$request['test'])->first();
			 // dd($test->id);
		$utf = Utf::where('user_id',Auth::user()->id)->where('test_id',$test->id)->first();
			// dd(Carbon::now()->toDateTimeString());
			// $ufb->lasttime = Carbon::now()->toDateTimeString();

			// dd(Carbon::now()->toDateTimeString()); 
		if(is_null($utf))
		{
			$utf = new Utf();
			$utf->user_id = Auth::user()->id;
			$utf->test_id = $test->id;
			$utf->count_answer = 1;
			$utf->ball = $test->ball;
			$utf->olimpiada_id = $test->olimpiada_id;
				// dd($utf->olimpiada_id);
				// $utf->gettime = Carbon::now()->toDateTimeString();

			if($request['answer']==$test->true_answer)
			{
				$utf->resoult = 1;	 
				$ufb->ball = $ufb->ball+$utf->ball;
				$ufb->count_true_answer = $ufb->count_true_answer+1;
				$test->true_answers = $test->true_answers+1;
				$test->save();
			}
			else
			{
				$utf->resoult = 0;
				$utf->ball = $utf->ball/2;
			}
			$ufb->count_answer =$ufb->count_answer+1; 
			$ufb->lasttime = Carbon::now()->toDateTimeString();
				// dd($ufb->lasttime);
			$ufb->save();
			$utf->gettime = Carbon::now()->toDateTimeString();
			$utf->save();
				// dd($utf);

			return redirect()->route('actionUserstatus',['id'=>$user->id]);

		}
		else
		{
			if($utf->count_answer==0)
			{
				$utf->count_answer = 1;

				if($request['answer']==$test->true_answer)
				{
					$ufb->count_true_answer = $ufb->count_true_answer+1;
					$ufb->ball = $ufb->ball+$utf->ball;
					$test->true_answers = $test->true_answers+1;
					$test->save();
					$utf->resoult = 1;
				}	
				else
				{
					$utf->ball = $utf->ball/2;
					$utf->resoult = 0;
				}
				$t=Carbon::now()->toDateTimeString();
					 // dd($t);
				$ufb->lasttime = $t;
					// dd($utf->lasttime);
				$ufb->count_answer =$ufb->count_answer+1;
				$ufb->save();
				$utf->gettime = Carbon::now()->toDateTimeString();
				$utf->save();
				return redirect()->route('actionUserstatus',['id'=>$user->id]);;
			}
			else
			{
				if($utf->count_answer==1 and $utf->resoult == 0)
				{
					$utf->count_answer = 2;

					if($request['answer']==$test->true_answer)
					{
						$ufb->ball = $ufb->ball+$utf->ball;
						$ufb->count_true_answer = $ufb->count_true_answer+1;
						$test->true_answers = $test->true_answers+1;
						$test->save();
						$utf->resoult = 1;
					}
					else
					{
						$utf->resoult = 0;
					}	
					$ufb->lasttime = Carbon::now()->toDateTimeString();
					$ufb->count_answer =$ufb->count_answer+1;
					$ufb->save();

					$utf->gettime = Carbon::now()->toDateTimeString();
					$utf->save();


					return redirect()->route('actionUserstatus',['id'=>$user->id]);
				}
				else
				{
					return redirect()->back()->with(['fail'=>'Siz bu savolga javob yo\'llab bo\'lgansiz']);
				}
			}

		}


	}

	public function getanswercomp(Request $request)
	{
		$comp = Competition::where('id',$request['comp'])->first();
		$test = Test::where('id',$request['test'])->first();
		$message = Message::where('competition_id',$comp->id)->first();
		$utfc = Utfc::where('user_id',Auth::user()->id)->where('competition_id',$comp->id)->where('test_id',$test->id)->first();
			// dd(Carbon::now()->toDateTimeString());
			// $ufb->lasttime = Carbon::now()->toDateTimeString();

			// dd(Carbon::now()->toDateTimeString()); 
		$comptime=date("Y-m-d H:i:s",strtotime($message->competition->begin_time) - strtotime('00:00') + strtotime($message->competition->length));
// dd($comptime . ' '.Carbon::now());
		$n = User::where('id',$message->user_1)->first()->id;
		$m = User::where('id',$message->user_2)->first()->id;

		$z=0;
		if(Auth::check() and (Auth::user()->id==$n or Auth::user()->id==$m)){
			$z=1;
		}
		if($comptime>=Carbon::now() and $z==1)
		{
			if(is_null($utfc))
			{
				$utfc = new Utfc();
				$utfc->user_id = Auth::user()->id;
				$utfc->test_id = $test->id;
				$utfc->count_answer = 1;
				$utfc->ball = $test->ball;
				$utfc->competition_id = $comp->id;
				// dd($utfc->olimpiada_id);
				// $utfc->gettime = Carbon::now()->toDateTimeString();

				if($request['answer']==$test->true_answer)
				{
					$utfc->resoult = 1;	 
					if($message->user_1==Auth::user()->id)
					{
						$comp->user_1_ball = $comp->user_1_ball+$utfc->ball;
					}
					else
					{
						$comp->user_2_ball = $comp->user_1_ball+$utfc->ball;

					}
					$comp->save();
					// $test->true_answers = $test->true_answers+1;
				}
				else
				{
					$utfc->resoult = 0;
					$utfc->ball = $utfc->ball/2;
				}
				$utfc->gettime = Carbon::now()->toDateTimeString();
				$utfc->save();
				// dd($utfc);

				return redirect()->route('actionCompetitionuserstatus',[
					'comp'=>$comp->id,
					'message'=>$message->id,

				]);

			}
			else
			{
				if($utfc->count_answer==0)
				{
					$utfc->count_answer = 1;

					if($request['answer']==$test->true_answer)
					{
						if($message->user_1==Auth::user()->id)
						{
							$comp->user_1_ball = $comp->user_1_ball+$utfc->ball;
						}
						else
						{
							$comp->user_2_ball = $comp->user_1_ball+$utfc->ball;

						}
						$comp->save();
						$utfc->resoult = 1;
					}	
					else
					{
						$utfc->ball = $utfc->ball/2;
						$utfc->resoult = 0;
					}
					$utfc->gettime = Carbon::now()->toDateTimeString();
					$utfc->save();
					return redirect()->route('actionCompetitionuserstatus',[
						'message'=>$message->id,
						'comp'=>$comp->id,
					]);
				}
				else
				{
					if($utfc->count_answer==1 and $utfc->resoult == 0)
					{
						$utfc->count_answer = 2;

						if($request['answer']==$test->true_answer)
						{
							if($message->user_1==Auth::user()->id)
							{
								$comp->user_1_ball = $comp->user_1_ball+$utfc->ball;
							}
							else
							{
								$comp->user_2_ball = $comp->user_1_ball+$utfc->ball;

							}
							$comp->save();
							$utfc->resoult = 1;
						}
						else
						{
							$utfc->resoult = 0;
						}				
						$utfc->gettime = Carbon::now()->toDateTimeString();
						$utfc->save();


						return redirect()->route('actionCompetitionuserstatus',[
							'comp'=>$comp->id,
							'message'=>$message->id,
						]);
					}
					else
					{
						return redirect()->back()->with(['fail'=>'Siz bu savolga javob yo\'llab bo\'lgansiz']);
					}
				}

			}
		}
		else
		{
			return redirect()->back()->with(['fail'=>'Ajratilgan vaqt tugab bo\'lgan !!!']);
		}

	}






	public function searchTest(Request $request)
	{
		$str = Input::get('searchtest');
		$res = "";
		$res1 = "";
			//dd(strtoupper($str[0]));
		for($i=0;$i<strlen($str);$i++){
			if($i == 0){
				$res .= strtoupper($str[$i]);
				$res1 .= strtolower($str[$i]);
			}
			else{
				$res .= $str[$i];
				$res1 .= $str[$i];
			}

		}
			///dd(Cache::get('subject_id'));
			//$ans = Test::where('subject_id',2);

		$alltests= Test::where('name_test','LIKE','%' .$res. '%')->orWhere('name_test','LIKE','%' .$res1. '%' )->orWhere('theme','LIKE','%' .$res. '%' )->orWhere('theme','LIKE','%' .$res1. '%' );

			//dd($tests);
		$tests = $alltests->where('subject_id',2)->paginate(13);
			  // dd($tests);
		return view($this->actionTest,[
			'tests' => $tests,
			
		]);

	}
	public function addanswer(Request $request)
	{
		$answer = new Answer();
		$answer->test_id = $request['test_id'];
		$answer->count_view = 0;
			// dd($answer);
		if($request->hasFile('answer'))
			{	$location = 'src/testanswers';	
		$file = Input::file('answer');
		$format = $file->getClientOriginalExtension();
		$timestamp = str_replace([' ',':'], '-',Carbon::now()->toDateTimeString());
		$name = $timestamp.'.'.$format;
		$path = public_path() . '/' . $location . '/';
		$file->move($path,$name);
		$path = $location.'/'.$name;
	}
			// dd($path);
	$answer->into_path = $path;

	$answer->save();
	$test = Test::where('id',$request['test_id'])->first();
	$test->have_answer = true;
	$test->save();
	return redirect()->route('adminalltests')->with(['fail'=>'Ma\'lumot qo\'shildi !!!']);
}
}


