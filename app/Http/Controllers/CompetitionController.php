<?php


namespace App\Http\Controllers;

use App\User;
use App\Test;
use App\Message;
use App\Subject;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\Competition;
use App\Utfc;
use Response;
class CompetitionController extends Controller
{
	
	public function addCompetition(Request $request)
	{
		 $user_1 = User::where('id',$request['user1'])->first();
		$user_2 = User::where('id',$request['user2'])->first();
		// dd($request['user_1']);
		$competition = new Competition();
		$competition->user_1_ball = 0;
		$competition->user_2_ball = 0;
		$competition->begin_time = Carbon::now()->toDateTimeString();
		$competition->length = $request['length'];
		$competition->count_tests = $request['count_tests'];
		$competition->subject_id = Subject::where('name',$request['subject'])->first()->id;

		if($request['ball']=='shunchaki')
			$competition->prize = 0;

		else
			$competition->prize = $request['ball'];
		// dd($competition);
	    $competition->save();
	    if($request['ball']=='shunchaki')
	    	$ball = 0;

		else
			$ball = $request['ball'];
		$message = new Message();
		$message->competition_id = $competition->id;
		$message->messages = 'Sizga '.$user_1->firstname.'dan chorlov keldi . <br>Fan : '.$request['subject'].'. <br>Testlar soni: '.$request['count_tests'].'  <br>Sharti : '.$ball .' balldan'; 
		// dd($message);
		$message->user_1 = $user_1->id;
		$message->user_2 = $user_2->id;
		$message->condition = 'so\'rovda';
		$message->save();
		$id =$message->id;
		return redirect()->route('actionWaitpanel',[
			'message'=>$id,
		]);

	}
	public function Compresoult($message = null)
	{
		$message = Message::where('id',$message)->first();
		// dd($message->competition_id);
		$user_1 = User::where('id',$message->user_1)->first();
		$user_2 = User::where('id',$message->user_2)->first();
		return view($this->actionCompetitionresoult,[
			'id'=>$message->competition_id,
			'user_1'=>$user_1,
			'user_2'=>$user_2,
			'message'=>$message->id
		]);
	}
	public function setmg()
	{
		$mg = Message::all();
		// $id = Message::where('user_id',Auth::user()->id)->first()->id;

		// dd($mg);
		 return Response::json(['mg' => $mg ]);
	}
	
}
