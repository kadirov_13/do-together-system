<?php

namespace App\Http\Controllers;

use App\Gpgroup;
use App\Gplcenter;
use Illuminate\Http\Request;
use Auth;
use App\Gpsch;
use App\Groupmessage;
use App\user;
/**
 * 
 */
class GroupsController extends Controller
{
	
	public function getGroup_gr(Request $request)
	{   
		if(is_null(Gpgroup::where('name',$request['name'])->orWhere('login',$request['login'])->first()))
		{


			if($request['name']=='' or $request['login']=='')
			{
				
				return redirect()->back()->with([ 'fail' =>'Ma\'lumotlar to\'liq kiritilmagan yoki xato kiritilgan!!!']);

			}
			else{
				if($request['password']==$request['conformpassword'])
				{
					$gpgroup = New Gpgroup;
					$gpgroup->name = $request['name'];
					$gpgroup->login = $request['login'];
					$gpgroup->password =  bcrypt($request['password']);
					$gpgroup->city = $request['city'];
					$gpgroup->image_id =1;
					$gpgroup->subject_id = 1;
					$gpgroup->reyting = 0;
					$gpgroup->admin_id = Auth::user()->id;
					$gpgroup->save();
					$group = Gpgroup::where('admin_id',Auth::user()->id)->first();
					if(is_null($group))
					{
						Auth::user()->gpgroup_id = $gpgroup->id;			
					}
					else
					{
						Auth::user()->gpgroup_id = $gpgroup->id;	
						Auth::user()->save();
						$usergp = User::where('gpgroup_id',$group->id)->first();
						if (is_null($usergp)) {
							$group->delete();
						}
						else{

							$group->admin_id = $usergp->id;
							$group->save();
						}
					}
					return redirect()->route('actionGroupinfo',['id'=>$gpgroup->id]);
				}
				else
				{
					return redirect()->back()->with(['fail' => 'Parolni qayta kiritishda xatolik bor. Qaytadan urinib ko\'ring!!! ']);
				}

			}
		}
		else{

			return redirect()->back()->with(['fail' => 'Bu nomli yoki loginli guruh  mavjud !!! .Iltimos boshqa nom va login tanlang.']);
		}


	}
	public function getmessagegroup($id = null,$type = null)
	{
		
		if($type == 1)
		{
			$groupmessage = new Groupmessage;
			$groupmessage->user_id = Auth::user()->id;
			$groupmessage->group_id = $id;
			$groupmessage->answer = -1;
			$groupmessage->save();
			return redirect()->back()->with('fail','So\'rovingiz jo\'natildi !!!');

		}	

	}
	public function actionGroupmessageanswer($answer=null,$id=null)
	{
		
		$message = Groupmessage::where('id',$id)->first();
		
		// dd($message);
		if($answer==0)
		{
			// $message->answer = 0;
			$message->delete();
			return redirect()->back()->with('fail','So\'rov rad etildi !!!');
		}
		else
		{ 
			$message->answer = 1;
			$user = User::where('id',$message->user_id)->first();
			// dd($message);
			$user->gpgroup_id = $message->group_id;
			$user->save();
			
			if(!is_null(Gpgroup::where('admin_id',$user->id)))
			{
				$gp = Gpgroup::where('admin_id',$user->id)->first();
				// dd($gp);
				$userpro = User::where('gpgroup_id',$gp->id)->first();
				if(!is_null($userpro))
				{
					$gp->admin_id = $userpro->id;
					$gp->save();
					dd($gp);
				}	
				else
				{
					$gp->delete();
				}		
			}
			
			$message->save();
			return redirect()->back()->with('fail','So\'rov qabul qilindi !!!');
		}
		
	}
}