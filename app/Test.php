<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    public function subject()
    {
    	return $this->hasOne('App\Subject','id','subject_id');
    }
    public function olimpiada()
    {
    	return $this->hasOne('App\Olimpiada','id','olimpiada_id');
    }
    
}
